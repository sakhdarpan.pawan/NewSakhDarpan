package com.microware.sakhdarpan.Object;

public class ComboBoxN {
	
	private int sID;
	private String sValue;
	private int iFlag;
	private String sLang;
	
	//---- ID-------
	public int getsID() {
		return sID;
	}
	public void setsID(int sID) {
		this.sID = sID;
	}
	
	//---- Value -------
	public String getsValue() {
		return sValue;
	}
	public void setsValue(String sValue) {
		this.sValue = sValue;
	}
	
	//---- Flag -------
	public int getiFlag() {
		return iFlag;
	}
	public void setiFlag(int iFlag) {
		this.iFlag = iFlag;
	}
	
	//---- Lang -------
	public String getsLang() {
		return sLang;
	}
	public void setsLang(String sLang) {
		this.sLang = sLang;
	}

}
