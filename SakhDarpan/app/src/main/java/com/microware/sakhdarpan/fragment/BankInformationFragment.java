package com.microware.sakhdarpan.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.microware.sakhdarpan.R;

/**
 * Created by CL_PAL on 10/4/2017.
 */

public class BankInformationFragment extends Fragment {

    ImageView btnSaveMember,btnClose;
    EditText eTIFSCCode,eTAccountNumber;
    Spinner spinPTG,spinDisability,spinDisabilityType,
            spinBankAccountInformation,spinBankName,spinBankBranchName;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.groupmemberbankinfo, container,
                false);

        eTIFSCCode = (EditText) rootView.findViewById(R.id.eTIFSCCode);
        eTAccountNumber = (EditText) rootView.findViewById(R.id.eTAccountNumber);
        btnSaveMember = (ImageView) rootView.findViewById(R.id.btnSaveMember);
        btnClose = (ImageView) rootView.findViewById(R.id.btnClose);


        spinPTG = (Spinner) rootView.findViewById(R.id.spinPTG);
        spinDisability = (Spinner) rootView.findViewById(R.id.spinDisability);
        spinDisabilityType = (Spinner) rootView.findViewById(R.id.spinDisabilityType);
        spinBankAccountInformation = (Spinner) rootView.findViewById(R.id.spinBankAccountInformation);
        spinBankName = (Spinner) rootView.findViewById(R.id.spinBankName);
        spinBankBranchName = (Spinner) rootView.findViewById(R.id.spinBankBranchName);
        spinPTG = (Spinner) rootView.findViewById(R.id.spinPTG);
        spinDisability = (Spinner) rootView.findViewById(R.id.spinDisability);
        spinDisabilityType = (Spinner) rootView.findViewById(R.id.spinDisabilityType);

        return rootView;

    }
}
