package com.microware.sakhdarpan.Activity;
import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import com.microware.sakhdarpan.DataProvider.DataProvider;
import com.microware.sakhdarpan.R;
import com.microware.sakhdarpan.fragment.ChangeVillageFragment;
import com.microware.sakhdarpan.fragment.ExportData;
import com.microware.sakhdarpan.fragment.GroupDetailsFragment;
import com.microware.sakhdarpan.fragment.HomeFragment;
import com.microware.sakhdarpan.fragment.MeetingDetailsFragment;
import com.microware.sakhdarpan.fragment.MprReportFragment;
import com.microware.sakhdarpan.fragment.ReportFragment;
import com.microware.sakhdarpan.fragment.SynchronizationFragment;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
public class DashboardNavigationActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener {
    private static String TAG = DashboardNavigationActivity.class.getSimpleName();
    Global global;
    DataProvider dataProvider;
    private Toolbar mToolbar;
    private FragmentDrawer drawerFragment;
    TextView tvusername, tvdistrictname;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.activitynavtabactivity);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
//        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        //getActionBar().setCustomView(R.layout.titlebarlogout);
        global = (Global) getApplication();
        dataProvider = new DataProvider(this);
          setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        drawerFragment = (FragmentDrawer)getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);
        tvusername = (TextView) findViewById(R.id.tvhamletvillage);
        tvusername.setText("John Michel");

        // display the first navigation drawer view on app launch
        if (savedInstanceState == null) {
            displayView(0);
        }


    }



    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            displayView(position);

        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    public boolean onCreateOptionsMenu(Menu menu) {
      /*  menu.add(0, 0, 0, global.getUser())
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        menu.add(0, 1, 1, "History").setIcon(R.drawable.home)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);*/
        return true;
    }

/*    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getOrder()) {

            case 1:
            Intent in = new Intent(DashboardNavigationActivity.this,DashBoard.class);
            finish();
            startActivity(in);
            break;
            default:
                break;
        }
        return true;
    }*/

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    public void displayView(int position) {
        Fragment fragment = null;
        String title = getString(R.string.app_name);
        switch (position) {

            case 0:
                fragment = new HomeFragment();
                title = getString(R.string.hmscrn);

                break;
            case 1:
                fragment = new GroupDetailsFragment();
                title = getString(R.string.groupdetails);

                break;
            case 2:
               // fragment = new MeetingDetailsFragment();
               Intent intent= new Intent(DashboardNavigationActivity.this,GroupMeeting_NavigationDrawer.class);
                startActivity(intent);
                title = getString(R.string.meetingDetails);

                break;
            case 3:
                fragment = new MprReportFragment();
                title = getString(R.string.mrpreport);

                break;
            case 4:
                fragment = new ReportFragment();
                title = getString(R.string.report);

                break;
            case 5:
                fragment = new ChangeVillageFragment();
                title = getString(R.string.changeVillage);

                break;
            case 6:
                fragment = new SynchronizationFragment();
                title = getString(R.string.synchronization);

                break;
            case 7:
               // fragment = new ExportData();
                Intent intent1= new Intent(DashboardNavigationActivity.this,MemberNavigationActivity.class);
                startActivity(intent1);
                title = getString(R.string.exportData);

                break;
            case 8:
               /* fragment = new SynchronizationFragment();
                title = getString(R.string.synchronization);*/
                CustomAlertSave(getResources().getString(R.string.logout));

                break;
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();

            // set the toolbar title

            getSupportActionBar().setTitle(title);
        }
    }
    public void CustomAlertSave(String msg) {
        // Create custom dialog object
        final Dialog dialog = new Dialog(this);
        // hide to default title for Dialog
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // inflate the layout dialog_layout.xml and set it as contentView
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_checklayout, null, false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        TextView txtTitle = (TextView) dialog
                .findViewById(R.id.txt_alert_message);
        txtTitle.setText(msg);

        Button btnyes = (Button) dialog.findViewById(R.id.btn_yes);
        Button btnno = (Button) dialog.findViewById(R.id.btn_no);

        btnyes.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent i = new Intent(DashboardNavigationActivity.this,
                        LoginActivity.class);
                finish();
                startActivity(i);

                dialog.dismiss();


            }
        });

        btnno.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                dialog.dismiss();

            }
        });

        // Display the dialog
        dialog.show();

    }
}



