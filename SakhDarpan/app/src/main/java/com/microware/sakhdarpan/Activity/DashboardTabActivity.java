package com.microware.sakhdarpan.Activity;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import com.microware.sakhdarpan.DataProvider.DataProvider;
import com.microware.sakhdarpan.R;
import com.microware.sakhdarpan.adapter.DashboardDrawerAdapter;
import com.microware.sakhdarpan.adapter.DashboardTabAdapter;

import java.util.Locale;

/**
 * Created by HP on 1/7/2017.
 */
@SuppressLint("NewApi")
public class DashboardTabActivity extends FragmentActivity {

    String navMenuTitles1[];

    int navIcon[] = {
            R.drawable.ic_homeviolet,
            R.drawable.ic_userviolet,
            R.drawable.ic_villageviolet,
            R.drawable.ic_reportviolet,
            R.drawable.ic_logoutviolet
    };

    int navgreenIcon[] = {
            R.drawable.ic_homegreen,
            R.drawable.ic_changeusergreen,
            R.drawable.ic_changevillagegreen,
            R.drawable.ic_reportgreen,
            R.drawable.ic_logoutgreen
    };

    String sState = "", sCluster = "", sDistrict = "", sBlock = "", sVillage = "";
    SharedPreferences sharedpreferences;

    DataProvider dataProvider;
    Global global;
    private CharSequence mDrawerTitle;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mTitle;
    private DashboardTabAdapter mAdapter;
    private ViewPager viewPager;
    TextView tvheading, tvhname;
    Button btnLogout;
    int pos = 0;
    GlobalLang glan = GlobalLang.getInstance();
    ToggleButton switchAB;
    TextView tvlanguage;
    private Locale myLocale;



    @SuppressLint("NewApi")
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stubAgricultureProductionFragment.java
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.dashboardtabactivity);
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getActionBar().setCustomView(R.layout.titlebarlogout);
        mTitle = mDrawerTitle = getTitle();
        dataProvider = new DataProvider(this);
        global = (Global) getApplication();

        sharedpreferences = getSharedPreferences("CMPSharedPreference", Context.MODE_PRIVATE);

        navMenuTitles1 = getResources().getStringArray(R.array.nav_drawer_labels);
        tvheading = (TextView) findViewById(R.id.tvheading);
 //       tvheading.setText(getResources().getString(R.string.dashboard));
        tvheading.setText(navMenuTitles1[pos]);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.list_slidermenu);
        View header = getLayoutInflater().inflate(R.layout.dashboardheader, null);

        TextView tvhhname = (TextView) header.findViewById(R.id.tvhhname);
        TextView tvhamlet = (TextView) header.findViewById(R.id.tvhamlet);
        TextView tvvillage = (TextView) header.findViewById(R.id.tvvillage);

        tvhamlet.setVisibility(View.GONE);
        mDrawerList.addHeaderView(header);
        mDrawerList.setAdapter(new DashboardDrawerAdapter(this, navMenuTitles1, navIcon, navgreenIcon, global.getLanguage(), 0));
//        btnLogout = (Button) findViewById(R.id.btnLogout);
//
//        btnLogout.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View arg0) {
//                // TODO Auto-generated method stub
//                CustomAlertSave(getResources().getString(R.string.logout));
//
//
//            }
//        });
        tvlanguage = (TextView)findViewById(R.id.tvlanguage);
        switchAB = (ToggleButton) findViewById(R.id.switchAB);
        SetLanguage();
//        SwitchScreen(2);
        switchAB.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // idtogleLang.setEnabled(true);
                if (switchAB.isChecked()) {
                    global.setLanguage(2);
//                    glan.setData("hi");
                    changeLang("hi");
//                    updateViews("hi");

                    Intent i = new Intent(DashboardTabActivity.this,DashboardTabActivity.class);
                    finish();
                    startActivity(i);
                    tvlanguage.setText("Hi");
                    Toast.makeText(getApplication(), "Hindi",
                            Toast.LENGTH_SHORT).show();
                } else {
                    global.setLanguage(1);
//                    glan.setData("en");
                    changeLang("en");
//                    updateViews("en");

                    Intent i = new Intent(DashboardTabActivity.this,DashboardTabActivity.class);
                    finish();
                    startActivity(i);
                    tvlanguage.setText("En");
                    Toast.makeText(getApplication(), "English",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
//        switchAB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView,
//                                         boolean isChecked) {
//                if (isChecked) {
//                    global.setLanguage(2);
////                    glan.setData("hi");
//                    changeLang("hi");
////                    updateViews("hi");
//                    Intent i = new Intent(DashboardTabActivity.this,DashboardTabActivity.class);
//                    finish();
//                    startActivity(i);
//                    Toast.makeText(getApplication(), "Hindi",
//                            Toast.LENGTH_SHORT).show();
//                } else {
//                    global.setLanguage(1);
////                    glan.setData("en");
//                    changeLang("en");
////                    updateViews("en");
//                    Intent i = new Intent(DashboardTabActivity.this,DashboardTabActivity.class);
//                    finish();
//                    startActivity(i);
//                    Toast.makeText(getApplication(), "English",
//                            Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.ham1, R.string.app_name, R.string.app_name) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mDrawerTitle);
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        viewPager = (ViewPager) findViewById(R.id.pager);
        mAdapter = new DashboardTabAdapter(getSupportFragmentManager());
        viewPager.setAdapter(mAdapter);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            public void onPageSelected(int position) {
                // on changing the page
                // make respected tab selected
                pos = position;

                mDrawerList.setAdapter(new DashboardDrawerAdapter(
                        DashboardTabActivity.this, navMenuTitles1, navIcon, navgreenIcon, global.getLanguage(), position));
                tvheading.setText(navMenuTitles1[pos]);
            }

            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            public void onPageScrollStateChanged(int arg0) {
            }
        });

        if(global.getLanguage()==1){
            tvlanguage.setText("En");
        }else if(global.getLanguage()==2) {
            tvlanguage.setText("Hi");
        }
        SetLocation();
        CheckCurrentVillage();
        SetUserDetails(tvhhname,tvvillage);

    }

    public void CheckCurrentVillage(){
        String Village = sharedpreferences.getString("VillageCode", "");
        if(Village.length()==0){
            SwitchScreen(2);
        }else if(global.getsGlobalVTSGUID()==null){
            String sql =  "Select VTSGUID from tblVillage_TS where VillageCode = '"+Village+"'";
            String sVTSGUID = dataProvider.getRecord(sql);
            global.setsGlobalVTSGUID(sVTSGUID);
        }
    }

    public void SetUserDetails(TextView tvHHName, TextView tvVillage){

        String Village = sharedpreferences.getString("VillageCode", "");

        String sql = "Select username from tblUserMaster where LoginID = '"+global.getiGlobalLoginID()+"'";
        String sUserName = dataProvider.getRecord(sql);
        if(sUserName.length()>0){
            tvHHName.setText(sUserName);
        }else{
            tvHHName.setText("");

        }
        String sqlvillage = "Select VillageName from tblMstVillage where VillageCode = '"+Village+"'";
        String sVillageName = dataProvider.getRecord(sqlvillage);

        if(sVillageName.length()>0){
            tvVillage.setText(sVillageName);
        }else{
            tvVillage.setText("");

        }
    }

    public void SetLocation(){
        sState = sharedpreferences.getString("StateCode", "");
        sDistrict = sharedpreferences.getString("DistrictCode", "");
        sBlock = sharedpreferences.getString("BlockCode", "");
        sCluster = sharedpreferences.getString("ClusterCode", "");
        sVillage = sharedpreferences.getString("VillageCode", "");

        if(sState.length()==0 && sDistrict.length()==0 && sBlock.length()==0 && sCluster.length()==0 && sVillage.length()==0){
            SwitchScreen(2);
        }else{
            SwitchScreen(0);
        }

    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }
    public void displayView(int iPosition) {

        if(iPosition == 4){
            CustomAlertSave(getResources().getString(R.string.logout));
        }else{
            viewPager.setCurrentItem(iPosition);
        }
        mDrawerLayout.closeDrawer(mDrawerList);

    }

    public void SwitchScreen(int pos) {
        mDrawerList.setAdapter(new DashboardDrawerAdapter(DashboardTabActivity.this,
                navMenuTitles1, navIcon, navgreenIcon, global.getLanguage(), pos));
        viewPager.setCurrentItem(pos);

    }

    public void PreviousScreen(int pos) {
        mDrawerList.setAdapter(new DashboardDrawerAdapter(DashboardTabActivity.this,
                navMenuTitles1, navIcon, navgreenIcon, global.getLanguage(), pos));
        viewPager.setCurrentItem(pos - 1);

    }


    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action bar actions click
        switch (item.getItemId()) {

            default:
                return super.onOptionsItemSelected(item);

        }

    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        // if nav drawer is opened, hide the action items
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        // menu.findItem(R.id.action_settings).setVisible(false);
        return super.onPrepareOptionsMenu(menu);
    }

    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    public void onBackPressed() {

    }

    public void CustomAlertSave(String msg) {
        // Create custom dialog object
        final Dialog dialog = new Dialog(this);
        // hide to default title for Dialog
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // inflate the layout dialog_layout.xml and set it as contentView
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_checklayout, null, false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        TextView txtTitle = (TextView) dialog
                .findViewById(R.id.txt_alert_message);
        txtTitle.setText(msg);

        Button btnyes = (Button) dialog.findViewById(R.id.btn_yes);
        Button btnno = (Button) dialog.findViewById(R.id.btn_no);

        btnyes.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent i = new Intent(DashboardTabActivity.this,
                        LoginActivity.class);
                finish();
                startActivity(i);

                dialog.dismiss();


            }
        });

        btnno.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                dialog.dismiss();

            }
        });

        // Display the dialog
        dialog.show();

    }

    public void changeLang(String lang) {
        if (lang.equalsIgnoreCase(""))
            return;
        myLocale = new Locale(lang);
        saveLocale(lang);
        Locale.setDefault(myLocale);
        Configuration config = new Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        updateTexts();
    }

    public void saveLocale(String lang) {
        String langPref = "Language";
        SharedPreferences prefs = getSharedPreferences("CommonPrefs",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(langPref, lang);
        editor.commit();
    }


    private void updateTexts() {
        tvheading.setText(navMenuTitles1[pos]);

    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.menudashboard, menu);
//        switchAB = (ToggleButton) menu.findItem(R.id.languagebutton)
//                .getActionView().findViewById(R.id.switchAB);
//        SetLanguage();
//        switchAB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView,
//                                         boolean isChecked) {
//                if (isChecked) {
//                    global.setLanguage(2);
//                    changeLang("hi");
//                    Toast.makeText(getApplication(), "Hindi",
//                            Toast.LENGTH_SHORT).show();
//                } else {
//                    global.setLanguage(1);
//                    changeLang("en");
//                    Toast.makeText(getApplication(), "English",
//                            Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//
//        return true;
//
//    }

    private void updateViews(String languageCode) {
        Context context = LocaleHelper.setLocale(this, languageCode);
        Resources resources = context.getResources();
//        tvheading.setText(resources.getString(R.string.dashboard));
        tvheading.setText(navMenuTitles1[pos]);
//        mTitleTextView.setText(resources.getString(R.string.main_activity_title));
//        mDescTextView.setText(resources.getString(R.string.main_activity_desc));
//        mAboutTextView.setText(resources.getString(R.string.main_activity_about));
//        mToTRButton.setText(resources.getString(R.string.main_activity_to_tr_button));
//        mToENButton.setText(resources.getString(R.string.main_activity_to_en_button));
//
//        setTitle(resources.getString(R.string.main_activity_toolbar_title));
    }

    public void SetLanguage() {
        if (global.getLanguage() == 1) {
            switchAB.setChecked(false);
            changeLang("en");
        } else if (global.getLanguage() == 2){
            switchAB.setChecked(true);
            changeLang("hi");
        }

    }
}
