package com.microware.sakhdarpan.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.microware.sakhdarpan.DataProvider.DataProvider;
import com.microware.sakhdarpan.R;
import com.microware.sakhdarpan.fragment.BankInformationFragment;
import com.microware.sakhdarpan.fragment.BasicInformationFragment;
import com.microware.sakhdarpan.fragment.IdentityInformationFragment;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;



public class MemberNavigationActivity extends AppCompatActivity implements FragmentMemberDrawer.FragmentDrawerListener {

    private static String TAG = MemberNavigationActivity.class.getSimpleName();
    Global global;
    DataProvider dataProvider;
    private Toolbar mToolbar;
    private FragmentMemberDrawer drawerFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activitynavmemberactivity);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        global = (Global) getApplication();
        dataProvider = new DataProvider(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        drawerFragment = (FragmentMemberDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);


        // display the first navigation drawer view on app launch
        if (savedInstanceState == null) {
            displayView(0);
        }
        //SetDetails(tvusername, tvdistrictname);

    }


    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            displayView(position);

        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    public boolean onCreateOptionsMenu(Menu menu) {
       // menu.add(0, 0, 0, global.getUser()).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
       // menu.add(0, 0, 0, global.getUser()).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        menu.add(0, 1, 1, "History").setIcon(R.drawable.home)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getOrder()) {

            case 1:
                //Intent in = new Intent(Baseline_Tab_Activity.this,DashBoard.class);
                Intent in = new Intent(MemberNavigationActivity.this,DashboardNavigationActivity.class);
                finish();
                startActivity(in);
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    public void displayView(int position) {
        Fragment fragment = null;
        String title = getString(R.string.app_name);
        switch (position) {
            case 0:

                fragment = new BasicInformationFragment();
                title = getString(R.string.baseline);

                break;
            case 1:
                fragment = new IdentityInformationFragment();
                title = getString(R.string.baseline1);

                break;
            case 2:
                fragment = new BankInformationFragment();
                title = getString(R.string.baseline2);

                break;

            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();

            // set the toolbar title

            getSupportActionBar().setTitle(title);
        }
    }
}




