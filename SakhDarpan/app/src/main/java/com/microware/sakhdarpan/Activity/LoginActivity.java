package com.microware.sakhdarpan.Activity;
import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.microware.sakhdarpan.DataProvider.DataProvider;
import com.microware.sakhdarpan.R;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

//import com.google.android.gms.appindexing.AppIndex;
//import com.google.android.gms.common.api.GoogleApiClient;
//import com.google.android.gms.common.api.PendingResult;
//import com.google.android.gms.common.api.ResultCallback;
//import com.google.android.gms.common.api.Status;
//import com.google.android.gms.location.LocationRequest;
//import com.google.android.gms.location.LocationServices;
//import com.google.android.gms.location.LocationSettingsRequest;
//import com.google.android.gms.location.LocationSettingsResult;
//import com.google.android.gms.location.LocationSettingsStatusCodes;

public class LoginActivity extends AppCompatActivity {
    private ProgressDialog progressDialog;
    Animation animation;
    ImageView imageView;
    EditText etUserName, etPassword;
    Button btnSubmit, btnexit;
    CheckBox idChek;
    Global global;
    DataProvider dataProvider;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String username = "", password = "";
    int iDownloadMaster = 0;

    JSONArray MstVillageArray = null;
    JSONArray tblMstBlockArray = null;
    JSONArray tblMstClusterArray = null;
    JSONArray tblMSTCommonArray = null;
    JSONArray tblMstDistrictArray = null;
    JSONArray tblMstStateArray = null;
    JSONArray UserMasterArray = null;
    JSONArray MstProjectArray = null;
    JSONArray MstPartnerLocationArray = null;
    JSONArray MstGroupMembersArray = null;
    JSONArray MstGroupArray = null;
    JSONArray MstNGOThemeArray = null;
    Animation shake;
//    String mPermission[] ={Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CAMERA};

//    GoogleApiClient client;
//    LocationRequest mLocationRequest;
//    PendingResult<LocationSettingsResult> result;

    private static final int PERMISSION_REQUEST_CODE = 200;
    int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;

    static final Integer LOCATION = 0x1;
    static final Integer CALL = 0x2;
    static final Integer WRITE_EXST = 0x3;
    static final Integer READ_EXST = 0x4;
    static final Integer CAMERA = 0x5;
    static final Integer ACCOUNTS = 0x6;
    static final Integer GPS_SETTINGS = 0x7;
    public final static int REQUEST_CODE = 10101;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        // requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.login_activity);
        // animation= AnimationUtils.loadAnimation(this, R.anim.anim_left);

        global = (Global) getApplication();
        dataProvider = new DataProvider(this);

        sharedPreferences = getSharedPreferences("CMPSharedPreference", Context.MODE_PRIVATE);
        imageView = (ImageView) findViewById(R.id.cmgimgid);
        // imageView.startAnimation(animation);
        // Handler handler=new Handler();

        etUserName = (EditText) findViewById(R.id.etUserName);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnexit = (Button) findViewById(R.id.btnClose);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();

        StrictMode.setThreadPolicy(policy);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        String folder_main = "CmFDoc";

        File f = new File(Environment.getExternalStorageDirectory(), folder_main);
        if (!f.exists()) {
            f.mkdirs();
            Toast.makeText(this, "Folder CmFDoc created", Toast.LENGTH_SHORT).show();
        } else {
//            Toast.makeText(this,"Folder CmF already exists",Toast.LENGTH_SHORT).show();
        }

        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.INTERNET, Manifest.permission.ACCESS_FINE_LOCATION};

        String[] perms = {"android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_INTERNAL_STORAGE", "android.permission.READ_INTERNAL_STORAGE", "android.permission.FINE_LOCATION", "android.permission.CAMERA", "android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.INTERNET", "android.permission.ACCESS_FINE_LOCATION"};
//String[] perms = { "android.permission.SYSTEM_ALERT_WINDOW"};

/* if(!hasPermissions(this, PERMISSIONS)){
   ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
}*/


        int permsRequestCode = 200;
        if (!hasPermissions(this, PERMISSIONS)) {
            if (checkDrawOverlayPermission()) {
                startService(new Intent(this, LoginActivity.class));
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(perms, permsRequestCode);
            }

        }


//        if(!hasPermissions(this, PERMISSIONS)){
//            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
//        }

        shake = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.shake);

        btnSubmit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                editor = sharedPreferences.edit();
//                editor.putInt("InitialDownloadCount", 0);
//                editor.putInt("FinalDownloadCount", 0);
                editor.putInt("CurrentDownloadCount", 0);
                editor.putInt("MaxDownloadCount", 0);
                editor.commit();
                String sUserName = "";
                String sPassword = "";

                if (etUserName.getText().toString().length() == 0) {
                    etUserName.setError(getResources().getString(R.string.Usernameblank));
                    etUserName.startAnimation(shake);
                    etUserName.requestFocus();
                    etUserName.performClick();
                } else if (etPassword.getText().toString().length() == 0) {
                    etPassword.setError(getResources().getString(R.string.Passwordblank));
                    etPassword.startAnimation(shake);
                    etPassword.requestFocus();
                    etPassword.performClick();
                }
                else if (etUserName.getText().toString().length() > 0 && etPassword.getText()
                        .toString().length() > 0) {


                    if (etUserName.getText().toString() != null
                            && etUserName.getText()
                            .toString().length() > 0) {
                        sUserName = etUserName.getText().toString();
                    }
                    if (etPassword.getText().toString() != null
                            && etPassword.getText()
                            .toString().length() > 0) {
                        sPassword = etPassword.getText().toString();
                    }

                            Intent i = new Intent(LoginActivity.this,
                                    DashboardNavigationActivity.class);

                            finish();
                            startActivity(i);


                        } else {
//                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.invaliduser),
//                                    Toast.LENGTH_SHORT).show();
                            CustomAlert(getResources().getString(R.string.validUsenamePassword));
                        }


            }
        });

        final CheckBox chek = (CheckBox) findViewById(R.id.idChek);

        chek.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (chek.isChecked()) {
                    etPassword.setInputType(InputType.TYPE_CLASS_TEXT);

                } else {

                    etPassword.setInputType(InputType.TYPE_CLASS_TEXT
                            | InputType.TYPE_TEXT_VARIATION_PASSWORD);

                }
            }
        });

        btnexit.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                finish();
                startActivity(intent);

            }
        });

    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean checkDrawOverlayPermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (!Settings.canDrawOverlays(this)) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, REQUEST_CODE);
            return false;
        } else {
            return true;
        }
    }

    @Override

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE) {
            if (Settings.canDrawOverlays(this)) {
                startService(new Intent(this, LoginActivity.class));
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {

        switch (permsRequestCode) {

            case 200:
                for (int i = 0; i < permissions.length; i++) {
                    boolean locationAccepted = grantResults[i] == PackageManager.PERMISSION_GRANTED;
                    // boolean cameraAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                }
                break;
        }
    }

    public void CustomAlert(String msg) {
        // Create custom dialog object
        final Dialog dialog = new Dialog(this);
        // hide to default title for Dialog
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // inflate the layout dialog_layout.xml and set it as contentView
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_layout, null, false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        TextView txtTitle = (TextView) dialog
                .findViewById(R.id.txt_alert_message);
        txtTitle.setText(msg);

        Button btnok = (Button) dialog.findViewById(R.id.btn_ok);
        btnok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Dismiss the dialog
                dialog.dismiss();

            }
        });

        // Display the dialog
        dialog.show();

    }

    public void GetAndroidID(){
        final TelephonyManager tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);

        final String tmDevice, tmSerial, tmPhone, androidId;
        tmDevice = "" + tm.getDeviceId();
        tmSerial = "" + tm.getSimSerialNumber();
        androidId = "" + android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

        UUID deviceUuid = new UUID(androidId.hashCode(), ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());
        String deviceId = deviceUuid.toString();
    }
    public void CustomAlertPermission(String msg) {
        // Create custom dialog object
        final Dialog dialog = new Dialog(this);
        // hide to default title for Dialog
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // inflate the layout dialog_layout.xml and set it as contentView
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_layout, null, false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        TextView txtTitle = (TextView) dialog
                .findViewById(R.id.txt_alert_message);
        txtTitle.setText(msg);

        Button btnok = (Button) dialog.findViewById(R.id.btn_ok);
        btnok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Dismiss the dialog
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
                dialog.dismiss();

            }
        });

        // Display the dialog
        dialog.show();

    }




    public void backup() {
        try {
//            File sdcard = Environment.getExternalStorageDirectory();
//            File outputFile = new File(sdcard, "cmf");
            String sdcard = Environment.getExternalStorageDirectory().toString() + "/" + "CmFDoc";
            File newFile = new File(sdcard);
            File outputFile = new File(newFile, "CmF");

            if (!outputFile.exists())
                outputFile.createNewFile();

            File data = Environment.getDataDirectory();
            File inputFile = new File(data, "data/" + getPackageName()
                    + "/databases/" + "cmf");
            InputStream input = new FileInputStream(inputFile);
            OutputStream output = new FileOutputStream(outputFile);
            byte[] buffer = new byte[1024];

            int length;
            while ((length = input.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }
            output.flush();
            output.close();
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
            throw new Error("Copying Failed");
        }
    }

    public void backupExternal() {
        String sdcard = "";
        File newFile;
        try {
            boolean mExternalStorageAvailable = false;
            boolean mExternalStorageWriteable = false;
            String state = Environment.getExternalStorageState();

            if (Environment.MEDIA_MOUNTED.equals(state)) {
                // We can read and write the media
                mExternalStorageAvailable = mExternalStorageWriteable = true;
                File sdCard = Environment.getExternalStorageDirectory();
                newFile = new File(sdCard.getAbsolutePath() + "/dir1/" + "CmFDoc");
                newFile.mkdirs();
//                File file = new File(newFile, "filename");
            } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
                // We can only read the media
                mExternalStorageAvailable = true;
                mExternalStorageWriteable = false;
                sdcard = Environment.getExternalStorageDirectory().toString() + "/" + "CmFDoc";
                newFile = new File(sdcard);
            } else {
                // Something else is wrong. It may be one of many other states, but all we need
                //  to know is we can neither read nor write
                mExternalStorageAvailable = mExternalStorageWriteable = false;
                sdcard = Environment.getExternalStorageDirectory().toString() + "/" + "CmFDoc";
                newFile = new File(sdcard);
            }

            File outputFile = new File(newFile, "CmF");

            if (!outputFile.exists())
                outputFile.createNewFile();

            File data = Environment.getDataDirectory();
            File inputFile = new File(data, "data/" + getPackageName()
                    + "/databases/" + "cmf");
            InputStream input = new FileInputStream(inputFile);
            OutputStream output = new FileOutputStream(outputFile);
            byte[] buffer = new byte[1024];

            int length;
            while ((length = input.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }
            output.flush();
            output.close();
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
            throw new Error("Copying Failed");
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }
}
