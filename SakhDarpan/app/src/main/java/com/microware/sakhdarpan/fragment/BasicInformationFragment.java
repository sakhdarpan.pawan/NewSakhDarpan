package com.microware.sakhdarpan.fragment;
        import android.app.AlertDialog;
        import android.app.Dialog;
        import android.content.Context;
        import android.content.DialogInterface;
        import android.content.Intent;
        import android.content.SharedPreferences;
        import android.graphics.drawable.ColorDrawable;
        import android.os.Bundle;
        import android.support.v4.app.Fragment;
        import android.text.Editable;
        import android.text.TextWatcher;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.view.Window;
        import android.widget.AdapterView;
        import android.widget.ArrayAdapter;
        import android.widget.Button;
        import android.widget.DatePicker;
        import android.widget.EditText;
        import android.widget.ImageView;
        import android.widget.Spinner;
        import android.widget.TextView;
        import android.widget.Toast;
        import com.microware.sakhdarpan.Activity.Global;
        import com.microware.sakhdarpan.DataProvider.DataProvider;
        import com.microware.sakhdarpan.Object.ComboBoxN;
        import com.microware.sakhdarpan.Object.ComboBoxT;
        import com.microware.sakhdarpan.R;
        import java.text.ParseException;
        import java.text.SimpleDateFormat;
        import java.util.ArrayList;
        import java.util.Calendar;
        import java.util.Date;
        import java.util.Locale;

public class BasicInformationFragment extends Fragment {
    DataProvider dataProvider;
    ArrayAdapter<String> adapter;
    ImageView btnSaveMember,btnClose;
    EditText eTMemberName,eTHusbandName,eTAge;
    TextView  tVMemberDateOfJoiningValue,tVDateOfleave,tVDateOfbirth;
    Spinner spinGender,spinEducation,spinStatus,spinDOB,spinPostInGroup;
    ArrayList<ComboBoxT> comboBoxStatus = new ArrayList<ComboBoxT>();
    private Locale myLocale;
    Dialog datepic;
    DatePicker datetext;
    //spinWBCat,spinBPL
    View rootView;
    private int mGMLYear;
    private int mGMLMonth;
    private int mGMLDay;
    static final int DATE_GM_DIALOG_LEAVING_ID = 3;

    TextView mGroupMemberJoiningDateDisplay;
    Button btnGroupMemberJoiningDate;
    private int mGMJDYear;
    private int mGMJDMonth;
    private int mGMJDDay;
    static final int DATE_GM_DIALOG_JD_ID = 4;

    private int iCurrentMemberSelectedPosition = -1;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
         rootView = inflater.inflate(R.layout.groupmemberbasicinfo, container,
                false);
        dataProvider = new DataProvider(getActivity());
        eTMemberName = (EditText) rootView.findViewById(R.id.eTMemberName);
        eTHusbandName = (EditText) rootView.findViewById(R.id.eTHusbandName);
        eTAge = (EditText) rootView.findViewById(R.id.eTAge);
        tVMemberDateOfJoiningValue = (TextView) rootView.findViewById(R.id.tVMemberDateOfJoiningValue);
        tVDateOfleave = (TextView) rootView.findViewById(R.id.tVDateOfleave);
        tVDateOfbirth = (TextView) rootView.findViewById(R.id.tVDateOfbirth);
        spinGender = (Spinner) rootView.findViewById(R.id.spinGender);
        spinEducation = (Spinner) rootView.findViewById(R.id.spinEducation);
        spinDOB = (Spinner) rootView.findViewById(R.id.spinDOB);
        spinPostInGroup = (Spinner) rootView.findViewById(R.id.spinPostInGroup);
        btnSaveMember = (ImageView) rootView.findViewById(R.id.btnSaveMember);
        btnClose = (ImageView) rootView.findViewById(R.id.btnClose);
        Global g12 = (Global)getActivity(). getApplication();
        String lang = g12.getsCurrentLanguage();
        if (lang == null) {
            lang = "en";
          //  btntg.setChecked(true);
            g12.setsCurrentLanguage("en");
        } else {
            if ("hi".equalsIgnoreCase(lang)) {
              //  btntg.setChecked(false);
                g12.setsCurrentLanguage("hi");
            } else {
               // btntg.setChecked(true);
                g12.setsCurrentLanguage("en");
            }
        }
        String sLanguage = "en";
        FillStatus(sLanguage);
        FillComboValueN(spinGender, sLanguage, 12);
        FillComboValueN(spinEducation, sLanguage, 8);
        FillComboValueN(spinPostInGroup, sLanguage, 1);
        eTMemberName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });
        btnSaveMember.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub

                int icheck = 0;

                icheck = sCheckValidation();
                if (icheck == 1) {
                    //saveNCDDetail();

                } else {
                    showAlert(icheck);
                }
            }
        });




        return rootView;

    }
    public int sCheckValidation() {
        try {
            if(eTMemberName.getText().toString().length()==0)
            {
                return 2;

            }
            if(spinGender.getSelectedItemPosition() == 0)
            {
                return 3;

            }
            if (eTHusbandName.getText().toString().length()==0) {
                return 4;
            }
            if (spinEducation.getSelectedItemPosition() == 0) {
                return 5;
            }
            if (spinPostInGroup.getSelectedItemPosition() == 0) {
                return 6;
            }

            if (spinStatus.getSelectedItemPosition() == 0) {
                return 7;
            }




        } catch (Exception e) {
            e.printStackTrace();
        }
        return 1;

    }
    public void showAlert(int iCheck) {
        if (iCheck == 2) {
            CustomAlertEdit(eTMemberName,getResources().getString(R.string.entername));
        } else if (iCheck == 3) {
            CustomAlertSpinner(spinGender,getResources().getString(R.string.selectgender));
        } else if (iCheck == 4) {
            CustomAlertEdit(eTHusbandName,getResources().getString(R.string.enterfatherneme));
        } else if (iCheck == 5) {
            CustomAlertSpinner(spinEducation,getResources().getString(R.string.selecteducation));
        } else if (iCheck == 6) {
            CustomAlertSpinner(spinPostInGroup,getResources().getString(R.string.selectpostin));
        } else if (iCheck == 7) {
            CustomAlertSpinner(spinStatus,getResources().getString(R.string.selectstatus));
        }
    }

    public void CustomAlertEdit(final EditText et, String msg) {
        // Create custom dialog object
        final Dialog dialog = new Dialog(getActivity());
        // hide to default title for Dialog
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // inflate the layout dialog_layout.xml and set it as contentView
        LayoutInflater inflater = (LayoutInflater) getActivity()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_layout, null, false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        TextView txtTitle = (TextView) dialog
                .findViewById(R.id.txt_alert_message);
        txtTitle.setText(msg);

        Button btnok = (Button) dialog.findViewById(R.id.btn_ok);
        btnok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Dismiss the dialog
                dialog.dismiss();
                et.performClick();
                et.requestFocus();
            }
        });

        // Display the dialog
        dialog.show();

    }

    public void CustomAlertSpinner(final Spinner spin, String msg) {
        // Create custom dialog object
        final Dialog dialog = new Dialog(getActivity());
        // hide to default title for Dialog
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // inflate the layout dialog_layout.xml and set it as contentView
        LayoutInflater inflater = (LayoutInflater) getActivity()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_layout, null, false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        TextView txtTitle = (TextView) dialog
                .findViewById(R.id.txt_alert_message);
        txtTitle.setText(msg);

        Button btnok = (Button) dialog.findViewById(R.id.btn_ok);
        btnok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Dismiss the dialog
                dialog.dismiss();
                spin.performClick();
                spin.requestFocus();
            }
        });

        // Display the dialog
        dialog.show();

    }
    private void FillComboValueN(Spinner spinCombo, String sLanguage, int iFlag) {
        ArrayList<ComboBoxN> comboBoxN = new ArrayList<ComboBoxN>();
        comboBoxN = dataProvider.getComboBoxN(sLanguage, iFlag);
        String sComboValue[] = new String[comboBoxN.size()];
        for (int i = 0; i < comboBoxN.size(); i++) {

            sComboValue[i] = comboBoxN.get(i).getsValue();

        }
        adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, sComboValue);
        spinCombo.setAdapter(adapter);

    }
    private void FillStatus(String sLanguage) {
        spinStatus = (Spinner) rootView.findViewById(R.id.sp_spinStatus);
        comboBoxStatus = dataProvider.getComboBoxT(sLanguage, 1);
        String sComboValue[] = new String[comboBoxStatus.size()];
        for (int i = 0; i < comboBoxStatus.size(); i++) {

            sComboValue[i] = comboBoxStatus.get(i).getsValue();

        }
        adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, sComboValue);
        spinStatus.setAdapter(adapter);

    }
    public void saveLocale(String lang) {
        String langPref = "Language";
        SharedPreferences prefs = getActivity().getSharedPreferences("CommonPrefs",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(langPref, lang);
        editor.commit();
    }

    public void DismischangeLang(String lang) {
        if (lang.equalsIgnoreCase(""))
            return;
        myLocale = new Locale(lang);
        saveLocale(lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
       getActivity(). getBaseContext().getResources().updateConfiguration(config,
               getActivity(). getBaseContext().getResources().getDisplayMetrics());
    }
}
