package com.microware.sakhdarpan.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.microware.sakhdarpan.Activity.Global;
import com.microware.sakhdarpan.DataProvider.DataProvider;
import com.microware.sakhdarpan.Object.GroupMeeting;
import com.microware.sakhdarpan.R;
import com.microware.sakhdarpan.adapter.GroupGridAdapter;

import java.util.ArrayList;
// https://www.sitepoint.com/mastering-complex-lists-with-the-android-recyclerview/

/**
 * Created by CL_PAL on 9/27/2017.
 */

public class GroupDetailsFragment extends Fragment implements View.OnClickListener {
    Global global;
    SwipeRefreshLayout mSwipeRefreshLayout;
    RecyclerView recyclerView;
    Button btn_add;
    DataProvider dataProvider;
   // GroupGridAdapter adapter;
    LinearLayoutManager layoutManager;
    View view;

    ArrayList<GroupMeeting> groupMeetings = new ArrayList<GroupMeeting>();
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.groupdetails,null);
        global = (Global) getActivity().getApplicationContext();
        mSwipeRefreshLayout = (SwipeRefreshLayout)view. findViewById(R.id.swipeToRefresh);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        //  mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        recyclerView = (RecyclerView)view.findViewById(R.id.recycler_view);

        dataProvider =new DataProvider(getActivity());
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
               // fillRecyclerdata();
                FillGroupName("55");
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });


      //  fillRecyclerdata();
        FillGroupName("55");

        return  view;
    }
    @Override
    public void onResume() {
        super.onResume();
       // fillRecyclerdata();
        FillGroupName("55");
    }


    @Override
    public void onClick(View v) {

    }
    public void FillGroupName(String sVillageCode) {

        dataProvider = new DataProvider(getActivity());
        Global g = (Global) getActivity().getApplication();
        String sLanguage = "";
        sLanguage = g.getsCurrentLanguage();
        if (sLanguage == null || sLanguage.length() == 0) {
            sLanguage = "en";
        }
        groupMeetings = dataProvider.getGroupMeeting(sVillageCode, sLanguage);
        if(groupMeetings!=null && groupMeetings.size()>0){


            GroupGridAdapter mAdapter = new GroupGridAdapter(getActivity(),groupMeetings);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        }



    }


}
