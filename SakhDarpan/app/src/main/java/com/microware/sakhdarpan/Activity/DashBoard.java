package com.microware.sakhdarpan.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.microware.sakhdarpan.R;

/**
 * Created by Jitender Kumar on 9/26/2017.
 */

public class DashBoard extends AppCompatActivity {

    Button btnlogin_link;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard);

        btnlogin_link = (Button)findViewById(R.id.btnlogin_link);

        btnlogin_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(DashBoard.this,LoginActivity.class);
                finish();
                startActivity(in);
            }
        });
    }
}
