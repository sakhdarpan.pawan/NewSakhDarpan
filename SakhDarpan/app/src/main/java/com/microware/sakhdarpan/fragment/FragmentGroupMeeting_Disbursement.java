package com.microware.sakhdarpan.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.microware.sakhdarpan.R;

/**
 * Created by microware on 29-09-2017.
 */

public class FragmentGroupMeeting_Disbursement extends Fragment {
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.navigation_fragment, container, false);
        Toast.makeText(getActivity(), "Fragment Dibursement", Toast.LENGTH_LONG).show();
        return rootView;
    }
}
