package com.microware.sakhdarpan.Activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.microware.sakhdarpan.R;
import com.microware.sakhdarpan.fragment.FragmentGroupMeeting_Bank;
import com.microware.sakhdarpan.fragment.FragmentGroupMeeting_Penalty;
import com.microware.sakhdarpan.fragment.FragmentGroupMeeting_BankLoan;
import com.microware.sakhdarpan.fragment.FragmentGroupMeeting_CashBox;
import com.microware.sakhdarpan.fragment.FragmentGroupMeeting_Summary;
import com.microware.sakhdarpan.fragment.FragmentGroupMeeting_Attendance;
import com.microware.sakhdarpan.fragment.FragmentGroupMeeting_Withdrawal;
import com.microware.sakhdarpan.fragment.FragmentGroupMeeting_Disbursement;
import com.microware.sakhdarpan.fragment.FragmentGroupMeeting_Repayment;
import com.microware.sakhdarpan.fragment.FragmentGroupMeeting_Saving;
import com.microware.sakhdarpan.R;
/**
 * Created by microware on 27-09-2017.
 */

public class GroupMeeting_NavigationDrawer extends AppCompatActivity {
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private View navHeader;

    private TextView txtName;
    private Toolbar toolbar;
    public static int navItemIndex = 0;

    // tags used to attach the fragments
    private static final String TAG_HOME = "home";
    private static final String TAG_FragmentOne = "fragmentOne";
    private static final String TAG_FragmentTwo = "fragmentTwo";
    private static final String TAG_FragmentThree = "fragmentThree";
    private static final String TAG_FragmentFour = "fragmentFour";
    private static final String TAG_FragmentFive = "fragmentFive";
    private static final String TAG_FragmentSix = "fragmentSix";
    private static final String TAG_FragmentSeven = "fragmentSeven";
    private static final String TAG_FragmentEight= "fragmentEight";
    private static final String TAG_FragmentNine = "fragmentNine";
    private static final String TAG_FragmentTen= "fragmentTen";
    public static String CURRENT_TAG = TAG_HOME;

    // toolbar titles respected to selected nav menu item
    private String[] activityTitles;

    // flag to load home fragment when user presses back key
    private boolean shouldLoadHomeFragOnBackPress = true;
    private Handler mHandler;
    TextView toolbar_title;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_meeting_navigationdrawer);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        toolbar_title=(TextView) toolbar.findViewById(R.id.toolbar_title);
        mHandler = new Handler();

        // Navigation view header
        navHeader = navigationView.getHeaderView(0);
        txtName = (TextView) navHeader.findViewById(R.id.name);
        // load toolbar titles from string resources
        activityTitles = getResources().getStringArray(R.array.nav_item_groupMeeting_activity_titles);
        loadNavHeader();
        // initializing navigation menu
        setUpNavigationView();

        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_HOME;
            loadHomeFragment();
        }
    }

    private void loadNavHeader() {
        // name, website
        txtName.setText("User");

        // showing dot next to notifications label
       // navigationView.getMenu().getItem(3).setActionView(R.layout.menu_dot);
    }
    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.nav_attendance:
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_HOME;
                        break;

                    case R.id.nav_Saving:
                        navItemIndex = 1;
                        CURRENT_TAG = TAG_FragmentOne;
                        break;
                    case R.id.nav_Rapayment:
                        navItemIndex = 2;
                        CURRENT_TAG = TAG_FragmentTwo;
                        break;
                    case R.id.nav_BankLoan:
                        navItemIndex = 3;
                        CURRENT_TAG = TAG_FragmentThree;
                        break;
                    case R.id.nav_Penalty:
                        // launch new intent instead of loading fragment
                        navItemIndex = 4;
                        CURRENT_TAG = TAG_FragmentFive;
                        drawer.closeDrawers();
                        break;
                    case R.id.nav_Disbursement:
                        // launch new intent instead of loading fragment
                        navItemIndex = 5;
                        CURRENT_TAG = TAG_FragmentSix;
                        drawer.closeDrawers();
                        break;
                    case R.id.nav_Withdrawal:
                        // launch new intent instead of loading fragment
                        navItemIndex = 6;
                        CURRENT_TAG = TAG_FragmentSeven;
                        drawer.closeDrawers();
                        break;
                    case R.id.nav_bank:
                        // launch new intent instead of loading fragment
                        navItemIndex = 7;
                        CURRENT_TAG = TAG_FragmentEight;
                        drawer.closeDrawers();
                        break;
                    case R.id.nav_summary:
                        // launch new intent instead of loading fragment
                        navItemIndex = 8;
                        CURRENT_TAG = TAG_FragmentNine;
                        drawer.closeDrawers();
                        break;
                    case R.id.nav_CashBox:
                        // launch new intent instead of loading fragment
                        navItemIndex = 9;
                        CURRENT_TAG = TAG_FragmentTen;
                        drawer.closeDrawers();
                        break;
                    default:
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_HOME;
                }

                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                loadHomeFragment();

                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }
    private void loadHomeFragment() {
        // selecting appropriate nav menu item
        selectNavMenu(0);

        // set toolbar title
        setToolbarTitle(0,"");

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawer.closeDrawers();

            // show or hide the fab button
            //toggleFab();
            return;
        }

        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                Fragment fragment = getHomeFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };

        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }

        // show or hide the fab button
        //toggleFab();

        //Closing drawer on item click
        drawer.closeDrawers();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }
    public void selectNavMenu(int nevIndex) {
        if(nevIndex==0) {
            navigationView.getMenu().getItem(navItemIndex).setChecked(true);
        }else{
            navigationView.getMenu().getItem(nevIndex).setChecked(true);
        }
    }
    public void setToolbarTitle(int nevIndex,String navtitle) {
        //getSupportActionBar().setTitle(activityTitles[navItemIndex]);
        if(nevIndex==0) {
            toolbar_title.setText(activityTitles[navItemIndex]);
        }else{
            toolbar_title.setText(navtitle);
        }
    }

    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                // home
                FragmentGroupMeeting_Attendance fragmentOne = new FragmentGroupMeeting_Attendance();
                return fragmentOne;

            case 1:
                // movies fragment
                FragmentGroupMeeting_Saving fragmentTwo = new FragmentGroupMeeting_Saving();
                return fragmentTwo;
            case 2:
                // notifications fragment
                FragmentGroupMeeting_Repayment fragmentThree = new FragmentGroupMeeting_Repayment();
                return fragmentThree;

            case 3:
                // settings fragment
                FragmentGroupMeeting_BankLoan fragmentFour = new FragmentGroupMeeting_BankLoan();
                return fragmentFour;
            case 4:
                // settings fragment
                FragmentGroupMeeting_Penalty fragmentFive = new FragmentGroupMeeting_Penalty();
                return fragmentFive;
            case 5:
                // settings fragment
                FragmentGroupMeeting_Disbursement fragmentSix = new FragmentGroupMeeting_Disbursement();
                return fragmentSix;
            case 6:
                // settings fragment
                FragmentGroupMeeting_Withdrawal fragmentSeven = new FragmentGroupMeeting_Withdrawal();
                return fragmentSeven;
            case 7:
                // settings fragment
                FragmentGroupMeeting_Bank fragmentEight = new FragmentGroupMeeting_Bank();
                return fragmentEight;
            case 8:
                // settings fragment
                FragmentGroupMeeting_Summary fragmentGroupMeeting_Summary = new FragmentGroupMeeting_Summary();
                return fragmentGroupMeeting_Summary;
            case 9:
                // settings fragment
                FragmentGroupMeeting_CashBox fragmentGroupMeeting_CashBox = new FragmentGroupMeeting_CashBox();
                return fragmentGroupMeeting_CashBox;

            default:
                return new FragmentGroupMeeting_Attendance();
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

        // This code loads home fragment when back key is pressed
        // when user is in other fragment than home
        if (shouldLoadHomeFragOnBackPress) {
            // checking if user is on other navigation menu
            // rather than home
            if (navItemIndex != 0) {
                navItemIndex = 0;
                CURRENT_TAG = TAG_HOME;
                loadHomeFragment();
                return;
            }
        }

        super.onBackPressed();
    }
}
