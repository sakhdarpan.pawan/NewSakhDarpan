package com.microware.sakhdarpan.Activity;


public class GlobalLang {
	private static GlobalLang instance;
	 
	   // Global variable
	   private String dataLang;
	 
	   // Restrict the constructor from being instantiated
	   private GlobalLang(){}
	 
	   public void setData(String d){
	     this.dataLang=d;
	   }
	   public String getData(){
	     return this.dataLang;
	   }
	 
	   public static synchronized GlobalLang getInstance(){
	     if(instance==null){
	       instance=new GlobalLang();
	     }
	     return instance;
	   }

}


