package com.microware.sakhdarpan.Activity;

import android.app.Application;

/**
 * Created by CL_PAL on 9/27/2017.
 */

import android.app.Application;

public class Global extends Application {

    private int language;
    private int iTotalMeetingTobeUploaded;
    private int iCurrentMeetingUploaded;
    private int iGroupBankInformation;
    private int iGroupTotalAttendance;
    private int iTabAttendance;
    private int iTabSaving;
    private int iTabRepayment;
    private int iTabPenalty;
    private int iTabDisbursement;
    private int iTabWithdrawal;
    private int iTabSummary;
    private int iTabCashBox;
    private int iTabBank;
    private String sCurrentLanguage;
    private String sCurrentUserName;
    private String sCurrentPassword;
    private int iCurrentLoginID;
    private int iRecordOverFlowFlag;

    private String sGlobalPanchayatCode;

    private String sGlobalDistrictName;
    private String sGlobalBlockName;
    private String sGlobalPanchayatName;
    private String sGlobalVillageName;
    private int iGlobalGroupCode;
    private String sGlobalGroupName;
    private String sSHGCode;
    private int iGlobalGroupCurrentMeetingNumber;
    private String sGlobalGroupCurrentMeetingDate;
    private int iGlobalGroupMaxMeetingNumber;
    private String sGlobalGroupMaxMeetingDate;
    private String sGlobalGroupMaxMeetingStatus;
    private int iGlobalGroupLastMeetingNumber;
    private String sGlobalGroupLastMeetingDate;
    private int iGlobalGroupCompulsorySaving;
    private String sGlobalGroupFormationDate;
    private String sGlobalGroupMeetingFrequency;
    private int iGlobalGroupMtgFrqVal;
    private int iGlobalGroupMtgFrq;
    private int iGlobalGroupComposition;
    private float iGlobalGroupCurrentMeetingOpeningBalance;
    private float iGlobalGroupCurrentMeetingClosingBalance;
    private float iGlobalGroupCurrentMeetingBankClosingBalance;
    private String sGroupBankAccountno;
    private int iCashinTransit;
    private int iNoOfmemberinGroup;
    private int iGroupComposition;
    private String sGroupBankOpeningDate;

    private String sGlobalHHGUID;
    private String sGlobalHHTSGUID;
    private String sGlobalHHFGUID;
    private String sGlobalHHPEGUID;
    private String sGlobalHHHSFGUID;
    private String sGlobalHHHSGUID;
    private String sGlobalHHMHGUID;
    private String sGlobalHHDGUID;
    private String sGlobalHHHPGUID;
    private String sGlobalHHDWGUID;
    private String sGlobalHHPDGUID;
    private String sGlobalHHPSGUID;
    private String sGlobalHHLGUID;
    private String sGlobalHHLPGUID;
    private String sGlobalHHGPGUID;
    private String sGlobalHHCGUID;
    private String sGlobalHHFIGUID;
    private String sGlobalHHNFGUID;
    private String sGlobalHHCropsGUID;
    private String sGlobalHHMigrationGUID;
    private String sGlobalHHSalariedGUID;
    private String sGlobalHHMGUID;
    private String sGlobalHHLoansGUID;
    private String sGlobalHHPAGUID;
    private String sGlobalHHPLGUID;
    private String sGlobalHHSBGUID;
    private String sGlobalHHIGUID;
    private String Year;
    private String sGlobalUserName;
    private String sGlobalPassword;
    private int iGlobalLoginID;
    private String sGlobalStateCode;
    private String sGlobalDistrictCode;
    private String sGlobalBlockCode;
    private String sGlobalClusterCode;
    private String sGlobalVillageCode;
    private int iGlobalFragmentFlag;
    private String sGlobalVTSGUID;
    private String sGlobalHamletGUID;
    private String sGlobalVIGUID;
    private String sGlobalVDWGUID;
    private String sGlobalVSGUID;
    private String sGlobalVNGUID;
    private String sGlobalVDGUID;
    private String sGlobalVCGUIDk;
    private String sGlobalVCGUIDr;
    private String sGlobalVCGUIDz;
    private String sGlobalVHGUID;
    private String sGlobalVMGUIDin;
    private String sGlobalVMGUIDout;
    private String sGlobalVPGUID;
    private String sGlobalHHLVSGUID;

    public String getsGlobalHHLVSGUID() {
        return sGlobalHHLVSGUID;
    }

    public void setsGlobalHHLVSGUID(String sGlobalHHLVSGUID) {
        this.sGlobalHHLVSGUID = sGlobalHHLVSGUID;
    }

    public String getsGlobalVPGUID() {
        return sGlobalVPGUID;
    }

    public void setsGlobalVPGUID(String sGlobalVPGUID) {
        this.sGlobalVPGUID = sGlobalVPGUID;
    }

    public String getsGlobalVMGUIDin() {
        return sGlobalVMGUIDin;
    }

    public void setsGlobalVMGUIDin(String sGlobalVMGUIDin) {
        this.sGlobalVMGUIDin = sGlobalVMGUIDin;
    }

    public String getsGlobalVMGUIDout() {
        return sGlobalVMGUIDout;
    }

    public void setsGlobalVMGUIDout(String sGlobalVMGUIDout) {
        this.sGlobalVMGUIDout = sGlobalVMGUIDout;
    }

    public String getsGlobalVHGUID() {
        return sGlobalVHGUID;
    }

    public void setsGlobalVHGUID(String sGlobalVHGUID) {
        this.sGlobalVHGUID = sGlobalVHGUID;
    }

    public String getsGlobalVDGUID() {
        return sGlobalVDGUID;
    }

    public void setsGlobalVDGUID(String sGlobalVDGUID) {
        this.sGlobalVDGUID = sGlobalVDGUID;
    }

    public String getsGlobalVCGUIDk() {
        return sGlobalVCGUIDk;
    }

    public void setsGlobalVCGUIDk(String sGlobalVCGUIDk) {
        this.sGlobalVCGUIDk = sGlobalVCGUIDk;
    }

    public String getsGlobalVCGUIDr() {
        return sGlobalVCGUIDr;
    }

    public void setsGlobalVCGUIDr(String sGlobalVCGUIDr) {
        this.sGlobalVCGUIDr = sGlobalVCGUIDr;
    }

    public String getsGlobalVCGUIDz() {
        return sGlobalVCGUIDz;
    }

    public void setsGlobalVCGUIDz(String sGlobalVCGUIDz) {
        this.sGlobalVCGUIDz = sGlobalVCGUIDz;
    }

    public String getsGlobalVNGUID() {
        return sGlobalVNGUID;
    }

    public void setsGlobalVNGUID(String sGlobalVNGUID) {
        this.sGlobalVNGUID = sGlobalVNGUID;
    }


    public String getsGlobalHHGUID() {
        return sGlobalHHGUID;
    }

    public void setsGlobalHHGUID(String sGlobalHHGUID) {
        this.sGlobalHHGUID = sGlobalHHGUID;
    }

    public String getsGlobalHHTSGUID() {
        return sGlobalHHTSGUID;
    }

    public void setsGlobalHHTSGUID(String sGlobalHHTSGUID) {
        this.sGlobalHHTSGUID = sGlobalHHTSGUID;
    }

    public String getsGlobalHHFGUID() {
        return sGlobalHHFGUID;
    }

    public void setsGlobalHHFGUID(String sGlobalHHFGUID) {
        this.sGlobalHHFGUID = sGlobalHHFGUID;
    }

    public String getsGlobalHHPEGUID() {
        return sGlobalHHPEGUID;
    }

    public void setsGlobalHHPEGUID(String sGlobalHHPEGUID) {
        this.sGlobalHHPEGUID = sGlobalHHPEGUID;
    }

    public String getsGlobalHHHSFGUID() {
        return sGlobalHHHSFGUID;
    }

    public void setsGlobalHHHSFGUID(String sGlobalHHHSFGUID) {
        this.sGlobalHHHSFGUID = sGlobalHHHSFGUID;
    }

    public String getsGlobalHHHSGUID() {
        return sGlobalHHHSGUID;
    }

    public void setsGlobalHHHSGUID(String sGlobalHHHSGUID) {
        this.sGlobalHHHSGUID = sGlobalHHHSGUID;
    }

    public String getsGlobalHHMHGUID() {
        return sGlobalHHMHGUID;
    }

    public void setsGlobalHHMHGUID(String sGlobalHHMHGUID) {
        this.sGlobalHHMHGUID = sGlobalHHMHGUID;
    }

    public String getsGlobalHHDGUID() {
        return sGlobalHHDGUID;
    }

    public void setsGlobalHHDGUID(String sGlobalHHDGUID) {
        this.sGlobalHHDGUID = sGlobalHHDGUID;
    }

    public String getsGlobalHHHPGUID() {
        return sGlobalHHHPGUID;
    }

    public void setsGlobalHHHPGUID(String sGlobalHHHPGUID) {
        this.sGlobalHHHPGUID = sGlobalHHHPGUID;
    }

    public String getsGlobalHHDWGUID() {
        return sGlobalHHDWGUID;
    }

    public void setsGlobalHHDWGUID(String sGlobalHHDWGUID) {
        this.sGlobalHHDWGUID = sGlobalHHDWGUID;
    }

    public String getsGlobalHHPDGUID() {
        return sGlobalHHPDGUID;
    }

    public void setsGlobalHHPDGUID(String sGlobalHHPDGUID) {
        this.sGlobalHHPDGUID = sGlobalHHPDGUID;
    }

    public String getsGlobalHHPSGUID() {
        return sGlobalHHPSGUID;
    }

    public void setsGlobalHHPSGUID(String sGlobalHHPSGUID) {
        this.sGlobalHHPSGUID = sGlobalHHPSGUID;
    }

    public String getsGlobalHHLGUID() {
        return sGlobalHHLGUID;
    }

    public void setsGlobalHHLGUID(String sGlobalHHLGUID) {
        this.sGlobalHHLGUID = sGlobalHHLGUID;
    }

    public String getsGlobalHHLPGUID() {
        return sGlobalHHLPGUID;
    }

    public void setsGlobalHHLPGUID(String sGlobalHHLPGUID) {
        this.sGlobalHHLPGUID = sGlobalHHLPGUID;
    }

    public String getsGlobalHHGPGUID() {
        return sGlobalHHGPGUID;
    }

    public void setsGlobalHHGPGUID(String sGlobalHHGPGUID) {
        this.sGlobalHHGPGUID = sGlobalHHGPGUID;
    }

    public String getsGlobalHHCGUID() {
        return sGlobalHHCGUID;
    }

    public void setsGlobalHHCGUID(String sGlobalHHCGUID) {
        this.sGlobalHHCGUID = sGlobalHHCGUID;
    }

    public String getsGlobalHHFIGUID() {
        return sGlobalHHFIGUID;
    }

    public void setsGlobalHHFIGUID(String sGlobalHHFIGUID) {
        this.sGlobalHHFIGUID = sGlobalHHFIGUID;
    }

    public String getsGlobalHHNFGUID() {
        return sGlobalHHNFGUID;
    }

    public void setsGlobalHHNFGUID(String sGlobalHHNFGUID) {
        this.sGlobalHHNFGUID = sGlobalHHNFGUID;
    }

    public String getsGlobalHHCropsGUID() {
        return sGlobalHHCropsGUID;
    }

    public void setsGlobalHHCropsGUID(String sGlobalHHCropsGUID) {
        this.sGlobalHHCropsGUID = sGlobalHHCropsGUID;
    }

    public String getsGlobalHHMigrationGUID() {
        return sGlobalHHMigrationGUID;
    }

    public void setsGlobalHHMigrationGUID(String sGlobalHHMigrationGUID) {
        this.sGlobalHHMigrationGUID = sGlobalHHMigrationGUID;
    }

    public String getsGlobalHHSalariedGUID() {
        return sGlobalHHSalariedGUID;
    }

    public void setsGlobalHHSalariedGUID(String sGlobalHHSalariedGUID) {
        this.sGlobalHHSalariedGUID = sGlobalHHSalariedGUID;
    }

    public String getsGlobalHHMGUID() {
        return sGlobalHHMGUID;
    }

    public void setsGlobalHHMGUID(String sGlobalHHMGUID) {
        this.sGlobalHHMGUID = sGlobalHHMGUID;
    }

    public String getsGlobalHHLoansGUID() {
        return sGlobalHHLoansGUID;
    }

    public void setsGlobalHHLoansGUID(String sGlobalHHLoansGUID) {
        this.sGlobalHHLoansGUID = sGlobalHHLoansGUID;
    }

    public String getsGlobalHHPAGUID() {
        return sGlobalHHPAGUID;
    }

    public void setsGlobalHHPAGUID(String sGlobalHHPAGUID) {
        this.sGlobalHHPAGUID = sGlobalHHPAGUID;
    }

    public String getsGlobalHHPLGUID() {
        return sGlobalHHPLGUID;
    }

    public void setsGlobalHHPLGUID(String sGlobalHHPLGUID) {
        this.sGlobalHHPLGUID = sGlobalHHPLGUID;
    }

    public String getsGlobalHHSBGUID() {
        return sGlobalHHSBGUID;
    }

    public void setsGlobalHHSBGUID(String sGlobalHHSBGUID) {
        this.sGlobalHHSBGUID = sGlobalHHSBGUID;
    }

    public String getsGlobalHHIGUID() {
        return sGlobalHHIGUID;
    }

    public void setsGlobalHHIGUID(String sGlobalHHIGUID) {
        this.sGlobalHHIGUID = sGlobalHHIGUID;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        Year = year;
    }

    public String getsGlobalUserName() {
        return sGlobalUserName;
    }

    public void setsGlobalUserName(String sGlobalUserName) {
        this.sGlobalUserName = sGlobalUserName;
    }

    public String getsGlobalPassword() {
        return sGlobalPassword;
    }

    public void setsGlobalPassword(String sGlobalPassword) {
        this.sGlobalPassword = sGlobalPassword;
    }

    public String getsGlobalClusterCode() {
        return sGlobalClusterCode;
    }

    public void setsGlobalClusterCode(String sGlobalClusterCode) {
        this.sGlobalClusterCode = sGlobalClusterCode;
    }




    public int getiGlobalFragmentFlag() {
        return iGlobalFragmentFlag;
    }

    public void setiGlobalFragmentFlag(int iGlobalFragmentFlag) {
        this.iGlobalFragmentFlag = iGlobalFragmentFlag;
    }


    public String getsGlobalHamletGUID() {
        return sGlobalHamletGUID;
    }

    public void setsGlobalHamletGUID(String sGlobalHamletGUID) {
        this.sGlobalHamletGUID = sGlobalHamletGUID;
    }

    public String getsGlobalVSGUID() {
        return sGlobalVSGUID;
    }

    public void setsGlobalVSGUID(String sGlobalVSGUID) {
        this.sGlobalVSGUID = sGlobalVSGUID;
    }

    public String getsGlobalVIGUID() {
        return sGlobalVIGUID;
    }

    public void setsGlobalVIGUID(String sGlobalVIGUID) {
        this.sGlobalVIGUID = sGlobalVIGUID;
    }

    public String getsGlobalVDWGUID() {
        return sGlobalVDWGUID;
    }

    public void setsGlobalVDWGUID(String sGlobalVDWGUID) {
        this.sGlobalVDWGUID = sGlobalVDWGUID;
    }
    public String getsGlobalVTSGUID() {
        return sGlobalVTSGUID;
    }

    public void setsGlobalVTSGUID(String sGlobalVTSGUID) {
        this.sGlobalVTSGUID = sGlobalVTSGUID;
    }
    public int getLanguage() {
        return language;
    }

    public int getiGlobalLoginID() {
        return iGlobalLoginID;
    }

    public void setiGlobalLoginID(int iGlobalLoginID) {
        this.iGlobalLoginID = iGlobalLoginID;
    }
    public void setLanguage(int language) {
        this.language = language;
    }
    //------Group Max Meeting Status----------
    public String getsGlobalGroupMaxMeetingStatus() {
        return sGlobalGroupMaxMeetingStatus;
    }
    public void setsGlobalGroupMaxMeetingStatus(
            String sGlobalGroupMaxMeetingStatus) {
        this.sGlobalGroupMaxMeetingStatus = sGlobalGroupMaxMeetingStatus;
    }

    public String getsGroupBankOpeningDate() {
        return sGroupBankOpeningDate;
    }
    public void setsGroupBankOpeningDate(String sGroupBankOpeningDate) {
        this.sGroupBankOpeningDate = sGroupBankOpeningDate;
    }
    public int getiNoOfmemberinGroup() {
        return iNoOfmemberinGroup;
    }
    public void setiNoOfmemberinGroup(int iNoOfmemberinGroup) {
        this.iNoOfmemberinGroup = iNoOfmemberinGroup;
    }
    public int getiCashinTransit() {
        return iCashinTransit;
    }
    public void setiCashinTransit(int iCashinTransit) {
        this.iCashinTransit = iCashinTransit;
    }
    /**Sep 1, 2014
     * Global.java
     * Get the value of Global.java
     * @return the sGroupBankAccountno
     */
    public String getsGroupBankAccountno() {
        return sGroupBankAccountno;
    }
    /**Sep 1, 2014
     * Global.java
     * Set the value of Global.java
     * @param sGroupBankAccountno the sGroupBankAccountno to set
     */
    public void setsGroupBankAccountno(String sGroupBankAccountno) {
        this.sGroupBankAccountno = sGroupBankAccountno;
    }
    public int getiGroupBankInformation() {
        return iGroupBankInformation;
    }
    public void setiGroupBankInformation(int iGroupBankInformation) {
        this.iGroupBankInformation = iGroupBankInformation;
    }
    public int getiGroupTotalAttendance() {
        return iGroupTotalAttendance;
    }
    public void setiGroupTotalAttendance(int iGroupTotalAttendance) {
        this.iGroupTotalAttendance = iGroupTotalAttendance;
    }
    //------ Attendance ----------
    public int getiTabAttendance() {
        return iTabAttendance;
    }
    public void setiTabAttendance(int iTabAttendance) {
        this.iTabAttendance = iTabAttendance;
    }

    //------ Saving ----------
    public int getiTabSaving() {
        return iTabSaving;
    }
    public void setiTabSaving(int iTabSaving) {
        this.iTabSaving = iTabSaving;
    }

    //------ Repayment ----------
    public int getiTabRepayment() {
        return iTabRepayment;
    }
    public void setiTabRepayment(int iTabRepayment) {
        this.iTabRepayment = iTabRepayment;
    }

    //------ Penalty ----------
    public int getiTabPenalty() {
        return iTabPenalty;
    }
    public void setiTabPenalty(int iTabPenalty) {
        this.iTabPenalty = iTabPenalty;
    }

    //------ Disbursement ----------
    public int getiTabDisbursement() {
        return iTabDisbursement;
    }
    public void setiTabDisbursement(int iTabDisbursement) {
        this.iTabDisbursement = iTabDisbursement;
    }

    //------ Withdrawal ----------
    public int getiTabWithdrawal() {
        return iTabWithdrawal;
    }
    public void setiTabWithdrawal(int iTabWithdrawal) {
        this.iTabWithdrawal = iTabWithdrawal;
    }

    //------ Summary ----------
    public int getiTabSummary() {
        return iTabSummary;
    }
    public void setiTabSummary(int iTabSummary) {
        this.iTabSummary = iTabSummary;
    }

    //------ CashBox ----------
    public int getiTabCashBox() {
        return iTabCashBox;
    }
    public void setiTabCashBox(int iTabCashBox) {
        this.iTabCashBox = iTabCashBox;
    }

    //------ Bank ----------
    public int getiTabBank() {
        return iTabBank;
    }
    public void setiTabBank(int iTabBank) {
        this.iTabBank = iTabBank;
    }

    //---Current Language------
    public String getsCurrentLanguage() {
        return sCurrentLanguage;
    }
    public void setsCurrentLanguage(String sCurrentLanguage) {
        this.sCurrentLanguage = sCurrentLanguage;
    }

    //---Login ID------
    public int getiCurrentLoginID() {
        return iCurrentLoginID;
    }
    public void setiCurrentLoginID(int iCurrentLoginID) {
        this.iCurrentLoginID = iCurrentLoginID;
    }

    //---Record OverFlow Flag------
    public int getiRecordOverFlowFlag() {
        return iRecordOverFlowFlag;
    }
    public void setiRecordOverFlowFlag(int iRecordOverFlowFlag) {
        this.iRecordOverFlowFlag = iRecordOverFlowFlag;
    }
    //------State Code----------
    public String getsGlobalStateCode() {
        return sGlobalStateCode;
    }
    public void setsGlobalStateCode(String sGlobalStateCode) {
        this.sGlobalStateCode = sGlobalStateCode;
    }

    //------District Code----------
    public String getsGlobalDistrictCode() {
        return sGlobalDistrictCode;
    }
    public void setsGlobalDistrictCode(String sGlobalDistrictCode) {
        this.sGlobalDistrictCode = sGlobalDistrictCode;
    }

    //------Block Code----------
    public String getsGlobalBlockCode() {
        return sGlobalBlockCode;
    }
    public void setsGlobalBlockCode(String sGlobalBlockCode) {
        this.sGlobalBlockCode = sGlobalBlockCode;
    }

    //------Panchayat Code----------
    public String getsGlobalPanchayatCode() {
        return sGlobalPanchayatCode;
    }
    public void setsGlobalPanchayatCode(String sGlobalPanchayatCode) {
        this.sGlobalPanchayatCode = sGlobalPanchayatCode;
    }

    //------Village Code----------
    public String getsGlobalVillageCode() {
        return sGlobalVillageCode;
    }
    public void setsGlobalVillageCode(String sGlobalVillageCode) {
        this.sGlobalVillageCode = sGlobalVillageCode;
    }

    //------District Name----------
    public String getsGlobalDistrictName() {
        return sGlobalDistrictName;
    }
    public void setsGlobalDistrictName(String sGlobalDistrictName) {
        this.sGlobalDistrictName = sGlobalDistrictName;
    }

    //------Block Name----------
    public String getsGlobalBlockName() {
        return sGlobalBlockName;
    }
    public void setsGlobalBlockName(String sGlobalBlockName) {
        this.sGlobalBlockName = sGlobalBlockName;
    }

    //------Panchayat Name----------
    public String getsGlobalPanchayatName() {
        return sGlobalPanchayatName;
    }
    public void setsGlobalPanchayatName(String sGlobalPanchayatName) {
        this.sGlobalPanchayatName = sGlobalPanchayatName;
    }

    //------Village Name----------
    public String getsGlobalVillageName() {
        return sGlobalVillageName;
    }
    public void setsGlobalVillageName(String sGlobalVillageName) {
        this.sGlobalVillageName = sGlobalVillageName;
    }

    //------Group Code----------
    public int getiGlobalGroupCode() {
        return iGlobalGroupCode;
    }
    public void setiGlobalGroupCode(int iGlobalGroupCode) {
        this.iGlobalGroupCode = iGlobalGroupCode;
    }

    //------Group Name----------
    public String getsGlobalGroupName() {
        return sGlobalGroupName;
    }
    public void setsGlobalGroupName(String sGlobalGroupName) {
        this.sGlobalGroupName = sGlobalGroupName;
    }

    //------SHG Code----------
    public String getsSHGCode() {
        return sSHGCode;
    }
    public void setsSHGCode(String sSHGCode) {
        this.sSHGCode = sSHGCode;
    }

    //------Group Current Meeting Number----------
    public int getiGlobalGroupCurrentMeetingNumber() {
        return iGlobalGroupCurrentMeetingNumber;
    }
    public void setiGlobalGroupCurrentMeetingNumber(
            int iGlobalGroupCurrentMeetingNumber) {
        this.iGlobalGroupCurrentMeetingNumber = iGlobalGroupCurrentMeetingNumber;
    }

    //------Group Current Meeting Date----------
    public String getsGlobalGroupCurrentMeetingDate() {
        return sGlobalGroupCurrentMeetingDate;
    }
    public void setsGlobalGroupCurrentMeetingDate(
            String sGlobalGroupCurrentMeetingDate) {
        this.sGlobalGroupCurrentMeetingDate = sGlobalGroupCurrentMeetingDate;
    }

    //------Group Max Meeting Number----------
    public int getiGlobalGroupMaxMeetingNumber() {
        return iGlobalGroupMaxMeetingNumber;
    }
    public void setiGlobalGroupMaxMeetingNumber(int iGlobalGroupMaxMeetingNumber) {
        this.iGlobalGroupMaxMeetingNumber = iGlobalGroupMaxMeetingNumber;
    }

    //------Group Max Meeting Date----------
    public String getsGlobalGroupMaxMeetingDate() {
        return sGlobalGroupMaxMeetingDate;
    }
    public void setsGlobalGroupMaxMeetingDate(String sGlobalGroupMaxMeetingDate) {
        this.sGlobalGroupMaxMeetingDate = sGlobalGroupMaxMeetingDate;
    }

    //------Group Last Meeting Number----------
    public int getiGlobalGroupLastMeetingNumber() {
        return iGlobalGroupLastMeetingNumber;
    }
    public void setiGlobalGroupLastMeetingNumber(
            int iGlobalGroupLastMeetingNumber) {
        this.iGlobalGroupLastMeetingNumber = iGlobalGroupLastMeetingNumber;
    }

    //------Group Last Meeting Date----------
    public String getsGlobalGroupLastMeetingDate() {
        return sGlobalGroupLastMeetingDate;
    }
    public void setsGlobalGroupLastMeetingDate(
            String sGlobalGroupLastMeetingDate) {
        this.sGlobalGroupLastMeetingDate = sGlobalGroupLastMeetingDate;
    }

    //------Group Compulsory Saving----------
    public int getiGlobalGroupCompulsorySaving() {
        return iGlobalGroupCompulsorySaving;
    }
    public void setiGlobalGroupCompulsorySaving(int iGlobalGroupCompulsorySaving) {
        this.iGlobalGroupCompulsorySaving = iGlobalGroupCompulsorySaving;
    }

    //------Group Formation Date----------
    public String getsGlobalGroupFormationDate() {
        return sGlobalGroupFormationDate;
    }
    public void setsGlobalGroupFormationDate(String sGlobalGroupFormationDate) {
        this.sGlobalGroupFormationDate = sGlobalGroupFormationDate;
    }

    //------Group Meeting Frequency----------
    public String getsGlobalGroupMeetingFrequency() {
        return sGlobalGroupMeetingFrequency;
    }
    public void setsGlobalGroupMeetingFrequency(
            String sGlobalGroupMeetingFrequency) {
        this.sGlobalGroupMeetingFrequency = sGlobalGroupMeetingFrequency;
    }

    //------Group MtgFrqVal----------
    public int getiGlobalGroupMtgFrqVal() {
        return iGlobalGroupMtgFrqVal;
    }
    public void setiGlobalGroupMtgFrqVal(int iGlobalGroupMtgFrqVal) {
        this.iGlobalGroupMtgFrqVal = iGlobalGroupMtgFrqVal;
    }

    //------Group MtgFrq----------
    public int getiGlobalGroupMtgFrq() {
        return iGlobalGroupMtgFrq;
    }
    public void setiGlobalGroupMtgFrq(int iGlobalGroupMtgFrq) {
        this.iGlobalGroupMtgFrq = iGlobalGroupMtgFrq;
    }

    //------Group Composition----------
    public int getiGlobalGroupComposition() {
        return iGlobalGroupComposition;
    }
    public void setiGlobalGroupComposition(int iGlobalGroupComposition) {
        this.iGlobalGroupComposition = iGlobalGroupComposition;
    }

    //------Group Current Meeting Opening Balance----------
    public float getiGlobalGroupCurrentMeetingOpeningBalance() {
        return iGlobalGroupCurrentMeetingOpeningBalance;
    }
    public void setiGlobalGroupCurrentMeetingOpeningBalance(
            float iGlobalGroupCurrentMeetingOpeningBalance) {
        this.iGlobalGroupCurrentMeetingOpeningBalance = iGlobalGroupCurrentMeetingOpeningBalance;
    }

    //------Group Current Meeting Closing Balance----------
    public float getiGlobalGroupCurrentMeetingClosingBalance() {
        return iGlobalGroupCurrentMeetingClosingBalance;
    }
    public void setiGlobalGroupCurrentMeetingClosingBalance(
            float iGlobalGroupCurrentMeetingClosingBalance) {
        this.iGlobalGroupCurrentMeetingClosingBalance = iGlobalGroupCurrentMeetingClosingBalance;
    }
    public int getiGroupComposition() {
        return iGroupComposition;
    }
    public void setiGroupComposition(int iGroupComposition) {
        this.iGroupComposition = iGroupComposition;
    }
    public String getsCurrentUserName() {
        return sCurrentUserName;
    }
    public void setsCurrentUserName(String sCurrentUserName) {
        this.sCurrentUserName = sCurrentUserName;
    }
    public String getsCurrentPassword() {
        return sCurrentPassword;
    }
    public void setsCurrentPassword(String sCurrentPassword) {
        this.sCurrentPassword = sCurrentPassword;
    }
    public float getiGlobalGroupCurrentMeetingBankClosingBalance() {
        return iGlobalGroupCurrentMeetingBankClosingBalance;
    }
    public void setiGlobalGroupCurrentMeetingBankClosingBalance(
            float iGlobalGroupCurrentMeetingBankClosingBalance) {
        this.iGlobalGroupCurrentMeetingBankClosingBalance = iGlobalGroupCurrentMeetingBankClosingBalance;
    }
    public int getiTotalMeetingTobeUploaded() {
        return iTotalMeetingTobeUploaded;
    }
    public void setiTotalMeetingTobeUploaded(int iTotalMeetingTobeUploaded) {
        this.iTotalMeetingTobeUploaded = iTotalMeetingTobeUploaded;
    }
    public int getiCurrentMeetingUploaded() {
        return iCurrentMeetingUploaded;
    }
    public void setiCurrentMeetingUploaded(int iCurrentMeetingUploaded) {
        this.iCurrentMeetingUploaded = iCurrentMeetingUploaded;
    }




}
