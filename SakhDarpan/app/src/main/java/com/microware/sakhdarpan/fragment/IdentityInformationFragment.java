package com.microware.sakhdarpan.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.microware.sakhdarpan.R;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
        import android.widget.EditText;
        import android.widget.ImageView;
        import android.widget.Spinner;
        import android.widget.TextView;

public class IdentityInformationFragment extends Fragment {
    ImageView btnSaveMember,btnClose;
    EditText eTMobileNumber,eTNregaNo,eTAadharNo,eTBPLCardNo;
    Spinner spinWBCat,spinBPL,spinMinority,spinMinorityReligion,spinSocialCategory;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.groupmemberidentityinfo, container, false);
        eTMobileNumber = (EditText) rootView.findViewById(R.id.eTMobileNumber);
        eTNregaNo = (EditText) rootView.findViewById(R.id.eTNregaNo);
        eTAadharNo = (EditText) rootView.findViewById(R.id.eTAadharNo);
        eTBPLCardNo = (EditText) rootView.findViewById(R.id.eTBPLCardNo);
        spinWBCat = (Spinner) rootView.findViewById(R.id.spinWBCat);
        spinBPL = (Spinner) rootView.findViewById(R.id.spinBPL);
        spinMinority = (Spinner) rootView.findViewById(R.id.spinMinority);
        spinMinorityReligion = (Spinner) rootView.findViewById(R.id.spinMinorityReligion);
        spinSocialCategory = (Spinner) rootView.findViewById(R.id.spinSocialCategory);
        btnSaveMember = (ImageView) rootView.findViewById(R.id.btnSaveMember);
        btnClose = (ImageView) rootView.findViewById(R.id.btnClose);

        btnSaveMember.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub

                int icheck = 0;

                icheck = sCheckValidation();
                if (icheck == 1) {
                    //saveNCDDetail();

                } else {
                    showAlert(icheck);
                }
            }
        });

        return rootView;

    }
    public int sCheckValidation() {
        try {

            if (spinWBCat.getSelectedItemPosition() == 0) {
                return 2;
            }
            if (spinSocialCategory.getSelectedItemPosition() == 0) {
                return 3;
            }





        } catch (Exception e) {
            e.printStackTrace();
        }
        return 1;

    }
    public void showAlert(int iCheck) {
        if (iCheck == 2) {
            CustomAlertSpinner(spinWBCat,getResources().getString(R.string.selectwbcat));
        } else if (iCheck == 3) {
            CustomAlertSpinner(spinSocialCategory,getResources().getString(R.string.selectsocialcateger));
        }
    }
    public void CustomAlertSpinner(final Spinner spin, String msg) {
        // Create custom dialog object
        final Dialog dialog = new Dialog(getActivity());
        // hide to default title for Dialog
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // inflate the layout dialog_layout.xml and set it as contentView
        LayoutInflater inflater = (LayoutInflater) getActivity()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_layout, null, false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        TextView txtTitle = (TextView) dialog
                .findViewById(R.id.txt_alert_message);
        txtTitle.setText(msg);

        Button btnok = (Button) dialog.findViewById(R.id.btn_ok);
        btnok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Dismiss the dialog
                dialog.dismiss();
                spin.performClick();
                spin.requestFocus();
            }
        });

        // Display the dialog
        dialog.show();

    }
}

