package com.microware.sakhdarpan.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.microware.sakhdarpan.Object.Movie;
import com.microware.sakhdarpan.R;
import com.microware.sakhdarpan.adapter.GroupMeeting_Attendance_Adapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by microware on 27-09-2017.
 */

public class FragmentGroupMeeting_Attendance extends Fragment {
    RecyclerView groupmeeting_Attendance_Recyclerview;
    ArrayList<Movie> movieList = new ArrayList<Movie>();
    LinearLayoutManager layoutManager;

    GroupMeeting_Attendance_Adapter groupMeeting_Attendance_Adapter;

    ImageView attendance_SaveBtn;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragmentgroupmeeting_attendance, container, false);

        groupmeeting_Attendance_Recyclerview=(RecyclerView)rootView.findViewById(R.id.groupmeeting_attendance_recyclerview);
        attendance_SaveBtn=(ImageView) rootView.findViewById(R.id.attendance_save_btn);
        fillRecyclerdata();

        attendance_SaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        fillRecyclerdata();
    }

    private void fillRecyclerdata() {
        movieList.clear();
        Movie movie = new Movie("Mad Max: Fury Road", "Action & Adventure", "2015");
        movieList.add(movie);

        movie = new Movie("Inside Out", "Animation, Kids & Family", "2015");
        movieList.add(movie);

        movie = new Movie("Star Wars: Episode VII - The Force Awakens", "Action", "2015");
        movieList.add(movie);

        movie = new Movie("Shaun the Sheep", "Animation", "2015");
        movieList.add(movie);

        movie = new Movie("The Martian", "Science Fiction & Fantasy", "2015");
        movieList.add(movie);

        movie = new Movie("Mission: Impossible Rogue Nation", "Action", "2015");
        movieList.add(movie);


        // registerlist = dataProvider.getDynamicVal("tblHorticultureInfo","");
        if(movieList!=null && movieList.size()>0){

            groupMeeting_Attendance_Adapter = new GroupMeeting_Attendance_Adapter(getActivity(),movieList);
            groupmeeting_Attendance_Recyclerview.setHasFixedSize(true);
            layoutManager = new LinearLayoutManager(getActivity());
            groupmeeting_Attendance_Recyclerview.setLayoutManager(layoutManager);
            groupmeeting_Attendance_Recyclerview.setItemAnimator(new DefaultItemAnimator());
            groupmeeting_Attendance_Recyclerview.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
            groupmeeting_Attendance_Recyclerview.setAdapter(groupMeeting_Attendance_Adapter);
            groupMeeting_Attendance_Adapter.notifyDataSetChanged();
        }

    }
}
