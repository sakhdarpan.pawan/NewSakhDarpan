package com.microware.sakhdarpan.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.microware.sakhdarpan.Activity.DashboardTabActivity;
import com.microware.sakhdarpan.R;

import java.util.Locale;

/**
 * Created by HP on 1/7/2017.
 */

public class DashboardDrawerAdapter extends BaseAdapter {
    String Titels[];
    String Titels1[];
    int Icon[];
    int navgreenIcon[];
    int iLanguageID;
    Context context;
    private Locale myLocale;
    int iPos;
    public DashboardDrawerAdapter(Context context, String Titels[], int Icon[], int navgreenIcon[], int iLanguageID, int iPos)
    {
        this.context=context;
        this.Titels=Titels;
        this.Icon = Icon;
        this.navgreenIcon = navgreenIcon;
        this.iPos=iPos;
        this.iLanguageID=iLanguageID;
    }


    public int getCount() {
        // TODO Auto-generated method stub
        return Titels.length;
    }


    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }


    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {
        View gridview=null;
        if(convertView==null){
            LayoutInflater layoutInflater=(LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            gridview = new View(context);
            gridview=layoutInflater.inflate(R.layout.cmf_drawer_adapter, null);
        }else{
            gridview=convertView;
        }
        TextView tvText=(TextView)gridview.findViewById(R.id.tvText1);
        LinearLayout layoutdrawer = (LinearLayout) gridview.findViewById(R.id.layoutdrawer);
//        CircleImageView CImgHead = (CircleImageView)gridview.findViewById(R.id.img_first);
//        TextView tvhhname=(TextView)gridview.findViewById(R.id.tvhhname);
//        TextView tvlocation=(TextView)gridview.findViewById(R.id.tvlocation);
        ImageView imgIcon = (ImageView) gridview.findViewById(R.id.imgIcon);
        TableRow tbltext=(TableRow)gridview.findViewById(R.id.tbltext);
 //       tvText.setText(Titels[position]);
        Titels1 = context.getResources().getStringArray(R.array.nav_drawer_labels);
        if(iLanguageID==1) {
            changeLang("en");
            tvText.setText(Titels1[position]);
        }else if(iLanguageID==2){
            changeLang("hi");
            tvText.setText(Titels1[position]);
        }else{
            changeLang("en");
            tvText.setText(Titels1[position]);
        }
        imgIcon.setBackgroundResource(Icon[position]);
        if(iPos==position)
        {

            tvText.setTextColor(context.getResources().getColor(R.color.themecmfgreen));
   //         tbltext.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.textsheet));
            layoutdrawer.setBackgroundColor(context.getResources().getColor(R.color.themecmfgreen));
            imgIcon.setBackgroundResource(navgreenIcon[position]);

        }




        tbltext.setOnClickListener(new View.OnClickListener() {


            public void onClick(View v) {
                // TODO Auto-generated method stub
                ((DashboardTabActivity)context).displayView(position);


            }
        });


        return gridview;
    }

    public void changeLang(String lang) {
        if (lang.equalsIgnoreCase(""))
            return;
        myLocale = new Locale(lang);
        saveLocale(lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        context.getResources().updateConfiguration(config,
                context.getResources().getDisplayMetrics());

        updateTexts();
    }

    public void saveLocale(String lang) {
        String langPref = "Language";
        SharedPreferences prefs = context.getSharedPreferences("CommonPrefs",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(langPref, lang);
        editor.commit();
    }


    private void updateTexts() {
//        tvText.setText(context.getResources().getString(R.string.dashboard));
    }
}
