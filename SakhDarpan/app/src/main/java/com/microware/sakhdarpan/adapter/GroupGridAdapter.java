package com.microware.sakhdarpan.adapter;

/**
 * Created by CL_PAL on 9/1/2017.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.microware.sakhdarpan.Activity.DashboardNavigationActivity;
import com.microware.sakhdarpan.Activity.MemberNavigationActivity;
import com.microware.sakhdarpan.DataProvider.DataProvider;
import com.microware.sakhdarpan.Object.GroupMeeting;
import com.microware.sakhdarpan.R;

import java.util.ArrayList;


public class GroupGridAdapter extends RecyclerView.Adapter<GroupGridAdapter.MyViewHolder> {
Context context;
   DataProvider dataProvider;
    CardView  card_view;
   /* int iGroupCode = 0;
    String sGroupName = null;
    int iGroupMeetingNumber = 0;
    String sGroupMeetingDate = null;*/
   TextView tvGroupCode,tvGroupName,tvGroupMeetingNumber,tvGroupMeetingDate ;
   int iGroupCode,iGroupMeetingNumber;
           String sGroupName,sGroupMeetingDate ;
    ArrayList<GroupMeeting> groupMeetings = new ArrayList<GroupMeeting>();


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public MyViewHolder(View view) {
            super(view);
            card_view = (CardView) itemView.findViewById(R.id.card_view);
            tvGroupName = ((TextView)itemView.findViewById(R.id.tViewGridGroupName));
            tvGroupCode = (TextView) itemView.findViewById(R.id.tViewGridGroupCode);
            tvGroupMeetingNumber =(TextView) itemView.findViewById(R.id.tViewGridGroupMeetingNumber);
            tvGroupMeetingDate = ((TextView) itemView.findViewById(R.id.tViewGridGroupMeetingDate));

        }
    }
    public GroupGridAdapter(Context context, ArrayList<GroupMeeting>groupMeetings) {
        this.context=context;
        this.groupMeetings=groupMeetings;

    }

    @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()) .inflate(R.layout.group_list_row, parent, false);
        dataProvider =new DataProvider(context);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int i) {

        tvGroupName.setText( groupMeetings.get(i).getGroupName());
        tvGroupCode.setText(String.valueOf(groupMeetings.get(i).getGroupCode()));
        tvGroupMeetingNumber.setText(String.valueOf(groupMeetings.get(i).getMeeetingNumber()));
        tvGroupMeetingDate.setText(String.valueOf(groupMeetings.get(i).getMeetingDate()));
        card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1= new Intent(context,MemberNavigationActivity.class);
                context.startActivity(intent1);
            }
        });

        // groupMeetings.get(i).getMeetingStatus();
       // groupMeetings.get(i).getLastMeeetingNumber();
    }

    @Override
    public int getItemCount() {
        return groupMeetings.size();
    }

}