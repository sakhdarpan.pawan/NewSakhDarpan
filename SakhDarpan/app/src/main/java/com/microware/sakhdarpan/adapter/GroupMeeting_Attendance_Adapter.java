package com.microware.sakhdarpan.adapter;

import android.content.Context;
import android.os.Environment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.microware.sakhdarpan.Activity.Global;
import com.microware.sakhdarpan.DataProvider.DataProvider;
import com.microware.sakhdarpan.Object.Movie;
import com.microware.sakhdarpan.R;

import java.io.File;
import java.util.ArrayList;

import static java.util.logging.Logger.global;

/**
 * Created by microware on 29-09-2017.
 */

public class GroupMeeting_Attendance_Adapter  extends RecyclerView.Adapter<GroupMeeting_Attendance_Adapter.MyViewHolder>{
        Context context;
        DataProvider dataprovider;
    Global global;


        ArrayList<Movie>CaseDetails;

public class MyViewHolder extends RecyclerView.ViewHolder {

    CardView card_view;
    LinearLayout layoutCardview;
    public TextView attendance_sno, attendance_memberName;
    ToggleButton attendance_btn;

    public MyViewHolder(View view) {
        super(view);

        card_view = (CardView) view.findViewById(R.id.card_view);
        attendance_sno = (TextView) view.findViewById(R.id.attendance_sno);
        attendance_memberName = (TextView) view.findViewById(R.id.attendance_memberName);
        attendance_btn = (ToggleButton) view.findViewById(R.id.attendance_btn);

    }

}

    public GroupMeeting_Attendance_Adapter(Context context, ArrayList<Movie> CaseDetails) {
        this.context = context;
        this.CaseDetails = CaseDetails;
        setHasStableIds(true);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_groupmeeting_attendance, parent, false);
        dataprovider = new DataProvider(context);
        global = (Global) context.getApplicationContext();

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

//        holder.idimage.setBackgroundResource(Icon[position]);

        holder.attendance_sno.setText("1");
        holder.attendance_memberName.setText("hello");
        holder.attendance_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.attendance_btn.isChecked())
                    Toast.makeText(context, "Present", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(context, "Absent", Toast.LENGTH_SHORT).show();
            }

        });
    }

    @Override
    public int getItemCount() {
        return CaseDetails.size();
    }
}
