package com.microware.sakhdarpan.DataProvider;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.microware.sakhdarpan.Object.ComboBoxN;
import com.microware.sakhdarpan.Object.ComboBoxT;
import com.microware.sakhdarpan.Object.GroupMeeting;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class DataProvider {
    static final int DATABASE_VERSION = 1;
    static final String DATABASE_NAME = "sakhdarpan";
    public SQLiteDatabase sqLiteDatabase;
 //   Global global;
    SharedPreferences sharedPreferences;
    private Cursor cursor;
    private Context context;
    private DatabaseHelper dbHelper;

    public DataProvider(Context _context) {
        try {
            this.context = _context;
            DatabaseHelper dbHelper;
   //         global = (Global) _context.getApplicationContext();
            sharedPreferences = _context.getSharedPreferences("CMPSharedPreference", Context.MODE_PRIVATE);

            dbHelper = DatabaseHelper.getInstance(context, DATABASE_NAME);
            sqLiteDatabase = dbHelper.getDatabase();
            cursor = null;
        } catch (Exception exp) {

        }

    }
    public void executeSql(String Sql) {
        sqLiteDatabase.beginTransaction();
        try {
            if (sqLiteDatabase == null) {
                sqLiteDatabase = dbHelper.getDatabase();
            }
            sqLiteDatabase.execSQL(Sql);
            sqLiteDatabase.setTransactionSuccessful();
        } catch (Exception exception) {
            Log.e("DataProvider",
                    "Error in executeSql :: " + exception.getMessage());
        } finally {
            sqLiteDatabase.endTransaction();
        }
    }

    public int getMaxRecord(String Sql) {
        int iIntegerValue = 0;
        try {
            cursor = null;
            if (sqLiteDatabase == null) {
                sqLiteDatabase = dbHelper.getDatabase();
            }
            cursor = sqLiteDatabase.rawQuery(Sql, null);
            if (cursor != null) {
                cursor.moveToFirst();
                while (cursor.isAfterLast() == false) {
                    iIntegerValue = cursor.getInt(0);
                    cursor.moveToNext();
                }
                cursor.close();
            }
        } catch (Exception exception) {
            Log.e("DataProvider",
                    "Error in getMaxRecord :: " + exception.getMessage());
        }
        return iIntegerValue;
    }

    public String getRecord(String Sql) {
        String sStringValue = "";
        try {
            cursor = null;
            if (sqLiteDatabase == null) {
                sqLiteDatabase = dbHelper.getDatabase();
            }
            cursor = sqLiteDatabase.rawQuery(Sql, null);
            if (cursor != null) {
                cursor.moveToFirst();
                while (cursor.isAfterLast() == false) {
                    sStringValue = cursor.getString(0);
                    cursor.moveToNext();
                }
                cursor.close();
            }
        } catch (Exception exception) {
            Log.e("DataProvider",
                    "Error in getRecord :: " + exception.getMessage());
        }
        return sStringValue;
    }

    public void deleteRecord(String sql) {

        try {
            executeSql(sql);
        } catch (Exception e) {
            System.out.println("error in deletng data  " + e);
        }
    }
    public ArrayList<GroupMeeting> getGroupMeeting(String sVillageID, String sLanguage) {
        ArrayList<GroupMeeting> groupMeetings = null;
        String sql = "";
        if (sLanguage.equalsIgnoreCase("en")) {
            sql = " SELECT  distinct mstGroup.GroupCode, mstGroup.Group_Name, dtMtg.MtgNo, dtMtg.MtgDate FROM mstGroup LEFT JOIN  dtMtg ON dtMtg.GroupCode = mstGroup.GroupCode where mstGroup.VillageCode ='"
                    + sVillageID
                    + "' Group By mstGroup.GroupCode Order by mstGroup.GroupCode  ";
        }

        cursor = null;
        try {
            if (sqLiteDatabase == null) {
                sqLiteDatabase = dbHelper.getDatabase();
            }
            cursor = sqLiteDatabase.rawQuery(sql, null);
            if (cursor != null) {
                groupMeetings = new ArrayList<GroupMeeting>();
                cursor.moveToFirst();
                while (cursor.isAfterLast() == false) {
                    GroupMeeting groupMeeting = new GroupMeeting();
                    groupMeeting.setGroupCode(cursor.getInt(0));
                    groupMeeting.setGroupName(cursor.getString(1));
                   /* if (cursor.getString(3) != null) {

                    } else {
                        groupMeeting.setMeeetingNumber(-1);
                    }*/
                    groupMeeting.setMeetingDate(cursor.getString(2));
                    groupMeeting.setMeetingStatus(cursor.getString(3));
                    groupMeetings.add(groupMeeting);
                    cursor.moveToNext();
                }
                cursor.close();
            }
            return groupMeetings;
        } catch (Exception exception) {
            Log.e("DataProvider",
                    "Error in getGroupMeeting :: " + exception.getMessage());
        }
        return groupMeetings;
    }
    public static long Daybetween() {
        String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
        String olddate = "01-01-1900";
        long days = 0;
        if (olddate != null && olddate.length() > 0 && date != null && date.length() > 0) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
            Date Date1 = null, Date2 = null;
            try {
                Date1 = sdf.parse(olddate);
                Date2 = sdf.parse(date);
                days = (Date2.getTime() - Date1.getTime()) / (24 * 60 * 60 * 1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return days;
        } else {
            return days;
        }
    }

    public ArrayList<GroupMeeting> getGroupTabInfo(int iGroupCode, int iMtgNo) {
        ArrayList<GroupMeeting> groupMeetings = null;
        String sql = "";
        sql = " SELECT GroupCode,MtgNo,MtgDate FROM dtMtg where GroupCode ='"
                + iGroupCode + "' and MtgNo='" + iMtgNo + "' ";
        cursor = null;
        try {
            if (sqLiteDatabase == null) {
                sqLiteDatabase = dbHelper.getDatabase();
            }
            cursor = sqLiteDatabase.rawQuery(sql, null);
            if (cursor != null) {
                groupMeetings = new ArrayList<GroupMeeting>();
                cursor.moveToFirst();
                while (cursor.isAfterLast() == false) {
                    GroupMeeting groupMeeting = new GroupMeeting();

                    groupMeeting.setGroupCode(cursor.getInt(0));
                    if (cursor.getString(2) != null) {
                        groupMeeting.setMeeetingNumber(cursor.getInt(1));
                    } else {
                        groupMeeting.setMeeetingNumber(-1);
                    }
                    groupMeeting.setMeetingDate(cursor.getString(2));
                    groupMeetings.add(groupMeeting);
                    cursor.moveToNext();
                }
                cursor.close();
            }
            return groupMeetings;
        } catch (Exception exception) {
            Log.e("DataProvider",
                    "Error in getGroupTabInfo :: " + exception.getMessage());
        }
        return groupMeetings;
    }

    public ArrayList<ComboBoxN> getComboBoxN(String sLanguage, int iFlag) {
        ArrayList<ComboBoxN> comboBoxNs = new ArrayList<ComboBoxN>();
        String sQueryComboBoxN = null;
        sQueryComboBoxN = "Select ID,Value,Flag,Lang from mstCombobox_N where Lang = '"
                + sLanguage + "' and Flag = " + iFlag + " Order by ID ";
        cursor = null;
        try {
            if (sqLiteDatabase == null) {
                sqLiteDatabase = dbHelper.getDatabase();
            }
            cursor = sqLiteDatabase.rawQuery(sQueryComboBoxN, null);
            if (cursor != null) {
                comboBoxNs = new ArrayList<ComboBoxN>();
                cursor.moveToFirst();
                while (cursor.isAfterLast() == false) {
                    ComboBoxN comboBoxN = new ComboBoxN();

                    comboBoxN.setsID(cursor.getInt(0));
                    comboBoxN.setsValue(cursor.getString(1));
                    comboBoxN.setiFlag(cursor.getInt(2));
                    comboBoxN.setsLang(cursor.getString(3));

                    comboBoxNs.add(comboBoxN);
                    cursor.moveToNext();
                }
                cursor.close();
            }
            return comboBoxNs;

        } catch (Exception exception) {
            Log.e("DataProvider",
                    "Error in getAllBank :: " + exception.getMessage());
        }
        return comboBoxNs;
    }
    public ArrayList<ComboBoxT> getComboBoxT(String sLanguage, int iFlag) {
        ArrayList<ComboBoxT> comboBoxTs = new ArrayList<ComboBoxT>();
        String sQueryComboBoxT = null;
        sQueryComboBoxT = "Select ID,Value,Flag,Lang from mstComboBox_T where Lang = '"
                + sLanguage + "' and Flag = " + iFlag + " Order by ID  ";

        //Select ID,Value,Flag,Lang from mstComboBox_T where Lang = "en" and Flag ="1" Order by ID
        cursor = null;
        try {
            if (sqLiteDatabase == null) {
                sqLiteDatabase = dbHelper.getDatabase();
            }
            cursor = sqLiteDatabase.rawQuery(sQueryComboBoxT, null);
            if (cursor != null) {
                comboBoxTs = new ArrayList<ComboBoxT>();
                cursor.moveToFirst();
                while (cursor.isAfterLast() == false) {
                    ComboBoxT comboBoxT = new ComboBoxT();

                    comboBoxT.setsID(cursor.getString(0));
                    comboBoxT.setsValue(cursor.getString(1));
                    comboBoxT.setiFlag(cursor.getInt(2));
                    comboBoxT.setsLang(cursor.getString(3));

                    comboBoxTs.add(comboBoxT);
                    cursor.moveToNext();
                }
                cursor.close();
            }
            return comboBoxTs;

        } catch (Exception exception) {
            Log.e("DataProvider",
                    "Error in getAllBank :: " + exception.getMessage());
        }
        return comboBoxTs;
    }
}
