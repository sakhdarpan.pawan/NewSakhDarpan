package com.microware.sakhdarpan.Object;

public class GroupMeeting {
    private int GroupCode;
    private String GroupName;
    private String SHGCode;
    private int MeeetingNumber;
    private String MeetingDate;
    private String MeetingStatus;
    private int LastMeeetingNumber;
    private String LastMeetingDate;

    // ------Group Code--------
    public int getGroupCode() {
        return GroupCode;
    }
    public void setGroupCode(int groupCode) {
        GroupCode = groupCode;
    }

    //-------Group Name----------
    public String getGroupName() {
        return GroupName;
    }
    public void setGroupName(String groupName) {
        GroupName = groupName;
    }

    //-------SHG Code-----
    public String getSHGCode() {
        return SHGCode;
    }
    public void setSHGCode(String sHGCode) {
        SHGCode = sHGCode;
    }

    //-------Meeting Number-----
    public int getMeeetingNumber() {
        return MeeetingNumber;
    }
    public void setMeeetingNumber(int meeetingNumber) {
        MeeetingNumber = meeetingNumber;
    }

    //-------Meeting Date-------
    public String getMeetingDate() {
        return MeetingDate;
    }
    public void setMeetingDate(String meetingDate) {
        MeetingDate = meetingDate;
    }

    //-------Meeting Status-------
    public String getMeetingStatus() {
        return MeetingStatus;
    }
    public void setMeetingStatus(String meetingStatus) {
        MeetingStatus = meetingStatus;
    }

    //-------Last Meeting Number---------
    public int getLastMeeetingNumber() {
        return LastMeeetingNumber;
    }
    public void setLastMeeetingNumber(int lastMeeetingNumber) {
        LastMeeetingNumber = lastMeeetingNumber;
    }



}
