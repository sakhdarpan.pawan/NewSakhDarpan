package com.microware.sakhdarpan.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.microware.sakhdarpan.fragment.GroupDetailsFragment;
import com.microware.sakhdarpan.fragment.MeetingDetailsFragment;
import com.microware.sakhdarpan.fragment.MprReportFragment;
import com.microware.sakhdarpan.fragment.ReportFragment;

/**
 * Created by HP on 1/7/2017.
 */

public class DashboardTabAdapter extends FragmentPagerAdapter {
    public DashboardTabAdapter(FragmentManager fm) {
        super(fm);
        // TODO Auto-generated constructor stub
    }

    @Override
    public Fragment getItem(int arg0) {
        // TODO Auto-generated method stub
        switch (arg0) {
            case 0:
                return new GroupDetailsFragment();
            case 1:
                return new MeetingDetailsFragment();
            case 2:
                return new MprReportFragment();
            case 3:
                return new ReportFragment();
//            case 4:
//                return new LogoutFragment();

        }
        return null;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return 4;
    }
}
